package naina.demo.naina;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ForgotPassword extends AppCompatActivity {
    private Button btFpSubmit;
    private EditText etFPEdittext;
    private String userInput,requestBody;
    private ProgressBar mProgressBar;

    public static final String Forgot_Paaswd_URL = "http://www.lagansarai.com/Service.svc/Forgot_Password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initializations();

        btFpSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                userInput = etFPEdittext.getText().toString().trim();

                if (userInput.equals("") || userInput.equals(null)){
                    Toast toast = Toast.makeText(ForgotPassword.this, "Please enter Username/Mobile/Email ", Toast.LENGTH_SHORT);
                    toast.setGravity(17,0,0);
                    toast.show();
                }
                else {
                    getPassword(userInput);
                }
            }
        });

    }


    private void initializations() {

        btFpSubmit = (Button) findViewById(R.id.btForgotPasswd);
        etFPEdittext = (EditText) findViewById(R.id.etFPInput);
        mProgressBar = (ProgressBar) findViewById(R.id.FPProgressBar);

    }

    //TODO:- JSON forgot password
    private void getPassword(String userinput) {

        showProgress(true);

        String userINPUT = userinput;
        JSONObject jsonObject = new JSONObject();
        RetryPolicy policy = new DefaultRetryPolicy(45000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        try {
            jsonObject.put("username", userINPUT);
            //jsonObject.put("UserID", userID);
            Log.d("JSONObject", "" + jsonObject);
            this.requestBody = jsonObject.toString();
        } catch (Exception ex) {
            Log.d("JSONObject", "excception" + ex);
        }
        StringRequest stringRequest = new StringRequest(1, Forgot_Paaswd_URL, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.d("Response from server", "" + response);
                try {
                    JSONArray jsonObject = new JSONArray(response);
                    int len = jsonObject.length();
                    JSONObject jsonObject1 = jsonObject.getJSONObject(0);
                    String loginStatus = jsonObject1.getString("status");
                    String message = jsonObject1.getString("Message");
                    Log.d("loginStatus", "" + loginStatus);
                    if (loginStatus.equals("1")) {

                        /*CharSequence msg = jsonObject1.getString("Message");
                        Log.d("msg", "" + msg);*/

                        showProgress(false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPassword.this);
                        builder.setTitle(" Alert ");
                        builder.setMessage("You will receive your password shortly through SMS.");
                        builder.setCancelable(false);
                        builder.setPositiveButton(" Got It ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(ForgotPassword.this,LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            }
                        });
                        builder.show();
                        return;

                    }else {

                        showProgress(false);
                        Toast toast = Toast.makeText(ForgotPassword.this, " "+message, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                //showProgress(false);
                error.printStackTrace();
            }
        }) {
            public byte[] getBody() throws AuthFailureError {
                byte[] bArr = null;
                try {
                    if (ForgotPassword.this.requestBody != null) {
                        bArr = ForgotPassword.this.requestBody.getBytes("utf-8");
                    }
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", ForgotPassword.this.requestBody, "utf-8");
                }
                return bArr;
            }
        };
        stringRequest.setRetryPolicy(policy);
        MySingleton.getInstance(this).addToRequest(stringRequest);
    }

    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.bringToFront();
            return;
        }
        Log.d("Inside Showprogress", "Elsepart");
        mProgressBar.setVisibility(View.GONE);
    }

}
