package naina.demo.naina;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.TimeZone;

public class ProjectEnquiryActivity extends AppCompatActivity {


    Spinner SpinnerAuthority, spinnerRate, spinnerVillage, spinnerZone;

    String getSpinner1;
//    private CheckBox chkIos;
    ArrayAdapter<CharSequence> adapter11;
    private EditText etEmail, etName,etMobileNo, etMobileNo2, etEmailID, etLocation, etExtract,etMap,etZone,etFSI, etDescription, etINR;
    private Button btRegSubmit;
    private RadioGroup rgInterested, rgLitigation, rgRate;
    private RadioButton rbInterested, rbLitigation, rbRate, rbSales, rbPurchase, rbJoint, rbSqft, rbSqm, rbGuntha, rbAcer, rbHector, rbYes, rbNo;
    private String getInterested="null", getLitigation="null", getRate="null";
    private String  name ="null",mobileNo ="null",mobileNo2 ="null", emailId ="null", location ="null", extract,map,zone,fsi ="null", description="null", spinnerAuth="null", selectedRate="null", selectedVillage="null", selectedZone="null", inr;
    private final String REGISTER_URL = "http://www.lagansarai.com/Service.svc/InsertEnquiry";
    private final String sendMobile_OTP = "http://www.lagansarai.com/Service.svc/Mobile_otp";
    JSONObject jsonObject;
    private TextView Sign_in_text;
    private ProgressBar mProgressBar;
    private ScrollView mRegScrollView;
    TimeZone tz;
    View view;
    String status;
    private String requestBody;
    private  String Log_id;
    private String file_name = "Users_Details";
    SharedPreferences sharedPref;
    private RadioButton tacAcceptRadioBtn, tacDeclineRadioBtn;



    //Least priority variables goes below....
    private final String TAG = "RegisterActivity";



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Enquiry");


        initialization();
        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        Log_id = sharedPref.getString("login_id","default");

//        addListenerOnChkIos();
        rgInterested.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rbInterested = (RadioButton) group.findViewById(checkedId);
                if (null != rbInterested && checkedId > -1) {
                    if(rbInterested.getText().equals("Sales")){
                        getInterested = "Sales";
                    }else if(rbInterested.getText().equals("Purchase")){
                        getInterested = "Purchase";
                    }else if(rbInterested.getText().equals("Joint Development")){
                        getInterested = "Joint Development";
                    }else {
                        getInterested = "null";
                    }
                    Toast.makeText(ProjectEnquiryActivity.this, rbInterested.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });
        rgLitigation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rbLitigation = (RadioButton) group.findViewById(checkedId);
                if (null != rbLitigation && checkedId > -1) {
                    if(rbLitigation.getText().equals("Yes")){
                        getLitigation = "1";
                    }else if(rbLitigation.getText().equals("No")){
                        getLitigation = "0";
                    }
                    Toast.makeText(ProjectEnquiryActivity.this, rbLitigation.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });
        rgRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rbRate = (RadioButton) group.findViewById(checkedId);
                if (null != rbRate && checkedId > -1) {
                    Toast.makeText(ProjectEnquiryActivity.this, rbRate.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });
//        SpinnerAuthority.setOnItemSelectedListener(this);
//        spinnerVillage.setOnItemSelectedListener(this);
//        spinnerZone.setOnItemSelectedListener(this);
//        spinnerRate.setOnItemSelectedListener(this);
//
//        SpinnerAuthority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });

        Calendar cal = Calendar.getInstance();
        tz = cal.getTimeZone();

        btRegSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                // Society = etSociety.getText().toString().trim();
                name = etName.getText().toString().trim();
//                extract = etExtract.getText().toString().trim();
                spinnerAuth = SpinnerAuthority.getSelectedItem().toString();
                selectedRate = spinnerRate.getSelectedItem().toString();
                selectedVillage = spinnerVillage.getSelectedItem().toString();
                selectedZone = spinnerZone.getSelectedItem().toString();
//                getInterested = rbInterested.getText().toString().trim();
//                getLitigation = rbLitigation.getText().toString().trim();
                getRate = etINR.getText().toString().trim();
//                map = etMap.getText().toString().trim();
                fsi = etFSI.getText().toString().trim();
//                zone = etZone.getText().toString().trim();
                inr = etINR.getText().toString().trim();
                description = etDescription.getText().toString().trim();
                mobileNo2 = etMobileNo2.getText().toString().trim();
                mobileNo = etMobileNo.getText().toString().trim();
                emailId = etEmailID.getText().toString().trim();
                location = etLocation.getText().toString().trim();

                // Editexts Validations
                if (spinnerAuth.equals(null) || spinnerAuth.equals("Select Type")){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Please Select Land Authority", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    SpinnerAuthority.requestFocus();
                    return;
                }
                if (selectedVillage.equals(null) || selectedVillage.equals("Select Village")){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Please Select Village", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    spinnerVillage.requestFocus();
                    return;
                }
                if (selectedZone.equals(null) || selectedZone.equals("Select Zone")){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Please Select Zone", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    spinnerZone.requestFocus();
                    return;
                }

                if (rgInterested.getCheckedRadioButtonId() == -1)
                {
                    // no radio buttons are checked
                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Please Select Your Interested Field", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                }
                if (rgLitigation.getCheckedRadioButtonId() == -1)
                {
                    // no radio buttons are checked
                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Please Select Litigation", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                }

                if (name.equals(null) || name.equals("")){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Name field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etName.requestFocus();
                    return;
                }
                if (inr.equals(null) || inr.equals("")){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Rate in INR field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etINR.requestFocus();
                    return;
                }

                if (mobileNo.equals(null) || mobileNo.equals("")){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Mobile field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etMobileNo.requestFocus();
                    return;
                }
                if (mobileNo.length()<10){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Enter valid mobile number.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etMobileNo.requestFocus();
                    return;
                }
                if (emailId.equals(null) || emailId.equals("")){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Email field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etEmailID.requestFocus();
                    return;
                }
                if (!(isValidEmail(emailId))){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Please enter valid email.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etEmailID.requestFocus();
                    return;
                }

                if (location.equals(null) || location.equals("")){

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Location field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etLocation.requestFocus();
                    return;
                }

                else {

                    enquiryJSON(getInterested,spinnerAuth, selectedRate, selectedVillage, selectedZone,name,location,mobileNo,
                            mobileNo2,emailId, fsi, getRate, getLitigation, description);
                   // btRegSubmit.setEnabled(false);
                }
            }
        });


    }

    private void enquiryJSON(String getInterested, String spinnerAuth, String selectedRate, String selectedVillage, String selectedZone, String name, String location,
                             String mobileNo, String mobileNo2, String emailId,
                             String fsi, String getRate, String getLitigation, String description) {
        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        showProgress(true);

        try {

            jsonObject.put("InterestedIn", getInterested);
            jsonObject.put("landautho", spinnerAuth);
            jsonObject.put("CompleteName", name);
            jsonObject.put("ResidenceLocation", location);
            jsonObject.put("ContactNo1", mobileNo);
            jsonObject.put("ContactNo2", mobileNo2);
            jsonObject.put("email", emailId);
            jsonObject.put("VillageOfLand", selectedVillage);
            jsonObject.put("Zone", selectedZone);
            jsonObject.put("FSI", fsi);
            jsonObject.put("ExpectedRate", getRate);
            jsonObject.put("areatype", selectedRate);
            jsonObject.put("Litigation", getLitigation);
            jsonObject.put("Details", description);
            jsonObject.put("LoginId", Log_id);
            // jsonObject.put("timezone", tz.getDisplayName());

            Log.d("JSONObject", "" + jsonObject);
            this.requestBody = jsonObject.toString();

        } catch (JSONException e) {

            Log.d("JSON Exception", " " + e);

        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "" + response);
                try {

                    showProgress(false);

                    JSONArray jSONArray = new JSONArray(response);
                    int len = jSONArray.length();
                    JSONObject jsonObject1 = jSONArray.getJSONObject(0);

                    String loginStatus = jsonObject1.getString("Status");
                    Log.d("loginStatus", "" + loginStatus);

                    String msg = jsonObject1.getString("Message");
                    Log.d("msg", "" + msg);

                    if (loginStatus.equals("3")) {
                        //Login Successful

                        showProgress(false);
                        Toast toast = Toast.makeText(ProjectEnquiryActivity.this," "+msg,Toast.LENGTH_LONG);
                        toast.setGravity(17, 0, 0);
                        toast.show();
                        // Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        // startActivity(intent);

                        return;
                    }
                    if (loginStatus.equals("1")) {
                        //mob already exists
                        showProgress(false);
                        Toast toast = Toast.makeText(ProjectEnquiryActivity.this," "+msg,Toast.LENGTH_LONG);
                        toast.setGravity(17, 0, 0);
                        toast.show();
//                        Intent enquiryIntent = new Intent(getApplicationContext(), ProjectEnquiryActivity.class);
//                        startActivity(enquiryIntent);
                        etName.setText("");
                        etLocation.setText("");
                        etEmailID.setText("");
                        etMobileNo.setText("");
                        etMobileNo2.setText("");
                        etDescription.setText("");
////                        etExtract.setText("");
//                        etFSI.setText("");
////                        etMap.setText("");
////                        etExtract.setText("");
//                        etZone.setText("");
                        return;
                    }if (loginStatus.equals("2")) {
                        //email already exists
                        showProgress(false);
                        Toast toast = Toast.makeText(ProjectEnquiryActivity.this," "+msg,Toast.LENGTH_LONG);
                        toast.setGravity(17, 0, 0);
                        toast.show();


                        return;
                    }


                    showProgress(false);

                    Toast toast = Toast.makeText(ProjectEnquiryActivity.this, "Network Error. Please try again.", Toast.LENGTH_SHORT);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    btRegSubmit.setEnabled(true);
                    Log.e("LoginActivity", "onResponse: Response from Server: " + response);

                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        MySingleton.getInstance(ProjectEnquiryActivity.this).addToRequest(stringRequest);

    }

    private void initialization() {

        // etSociety = (EditText)findViewById(R.id.etSocietyName);
        etName = (EditText)findViewById(R.id.etName);
        etMobileNo = (EditText)findViewById(R.id.etMobileNo);
        SpinnerAuthority = (Spinner) findViewById(R.id.SpinnerAuthority);
        spinnerRate = (Spinner) findViewById(R.id.SpinnerRate);
        spinnerVillage = (Spinner) findViewById(R.id.SpinnerVillage);
        spinnerZone = (Spinner) findViewById(R.id.SpinnerZone);
        etMobileNo2 = (EditText)findViewById(R.id.etMobileNo2);
        etEmailID = (EditText)findViewById(R.id.etEmail);
        etLocation = (EditText)findViewById(R.id.etLocation);
        // etUserName = (EditText)findViewById(R.id.etregUserName);
//        etExtract = (EditText)findViewById(R.id.etExtract);
        etMap = (EditText)findViewById(R.id.etMap);
//        etZone = (EditText)findViewById(R.id.etZone);
        etFSI = (EditText)findViewById(R.id.etFSI);
        etINR = (EditText)findViewById(R.id.etINR);
        btRegSubmit = (Button) findViewById(R.id.btRegSubmit);
//        Sign_in_text = (TextView) findViewById(R.id.tvSignInText);
        mProgressBar = (ProgressBar) findViewById(R.id.registerProgressBar);
        mRegScrollView = (ScrollView) findViewById(R.id.regScroll);
        etDescription = (EditText) findViewById(R.id.etDescription);
        jsonObject = new JSONObject();

        rgLitigation = (RadioGroup) findViewById(R.id.rgLitigation);
        rgInterested = (RadioGroup) findViewById(R.id.rgInterested);
        rgRate = (RadioGroup) findViewById(R.id.rgRate);


//        tacAcceptRadioBtn.setOnCheckedChangeListener(this);
//        tacDeclineRadioBtn.setOnCheckedChangeListener(this);
//        tacDeclineRadioBtn.setChecked(true);//Bcoz by default the Register button should be freezed.

    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    //TODO:- Register JSON


    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");
            this.mProgressBar.setVisibility(View.VISIBLE);
            this.mProgressBar.bringToFront();
            //this.mRegScrollView.setVisibility(View.GONE);
            return;
        }
        Log.d("Inside Showprogress", "Elsepart");
        this.mProgressBar.setVisibility(View.GONE);
        this.mRegScrollView.setVisibility(View.VISIBLE);
        this.mRegScrollView.bringToFront();
    }





}
