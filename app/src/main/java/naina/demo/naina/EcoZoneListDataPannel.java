package naina.demo.naina;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class EcoZoneListDataPannel {
    public static LinkedHashMap<String, List<String>> getData() {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();



        List<String> panel2 = new ArrayList<String>();
        panel2.add("15_khalapur_taluka");
        panel2.add("19_Panvel_Taluka");
        panel2.add("20_Panvel_taluka");
        panel2.add("21_Panvel_khalapur_taluka");
        panel2.add("22_khalapur_taluka");
        panel2.add("23_Karjat_Taluka");
        panel2.add("27_Panvel_Taluka");
        panel2.add("28_Panvel_Taluka");
        panel2.add("29_Karjat_Taluka");
        panel2.add("31_Panvel_Taluka");
        panel2.add("31_Ambernath_Panvel_Talukas");
        panel2.add("32_Panvel_Taluka_Sanctioned_MAP");
        panel2.add("33_Ambernath_Taluka");
        panel2.add("34_Ambernath_Karjat_Taluka");
        panel2.add("index_sheet_showing");
        panel2.add("List_of_affected_survey");
        panel2.add("MESZ_Land_use_map");
        panel2.add("tourism-master-plan-for-matheran");
        panel2.add("ZMP_Report_for_Matheran");
        panel2.add("Zonal_Master_Plan");

        List<String> panel3 = new ArrayList<String>();
        panel3.add("Sanctioned_Development_Plan_Matheran_Council");
        panel3.add("Sanctioned_Development_Plan_Bazaar_Plot");
        panel3.add("Sanctioned_Development_Plan_rep__Matheran_Council");
        panel3.add("Sanctioned_Development_Control_Regulations_Matheran_Council");

        List<String> panel4 = new ArrayList<String>();
        panel4.add("Amendment S.O.83(E) dated 16.01.2004 on Matheran Ecosensitive Area");
        panel4.add("ESZ Notification 04-02-2003");
        panel4.add("Sanctioned Amended Notification Zonal Master Plan Matheran ESZ-07-08-2019");

        expandableListDetail.put("Zonal Master Plan for Matheran", panel2);
        expandableListDetail.put("Sub-Zonal Master Plan for Matheran", panel3);
        expandableListDetail.put("Notification of ESZ ", panel4);

        return expandableListDetail;
    }
}
