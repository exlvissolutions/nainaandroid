package naina.demo.naina;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Feedback extends AppCompatActivity {
    EditText Desc;
    Button button;
    String desc;
    String HttpUrl = "http://www.lagansarai.com/Service.svc/feedback";
    SharedPreferences sharedPref;
    private String file_name = "Users_Details";
    private String TAG = "Feedback";
    String Log_ID, name, loginStatus;
    ProgressDialog progressDialog;
    String predefinedText = "To, The President, NAINA Builders Welfare Association\n\n"+ // comment by amitabh 14aug 2020
                             "Subject : ";
    //  "Email id: nainabuildersassociation@gmail.com \n" +
          //  "Mobile no: +91 9167213907\n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        // userName = sharedPref.getString("Username","default");
        // loginStat = sharedPref.getString("Login_Status","default");
        // userType = sharedPref.getString("UserType","default");
        name = sharedPref.getString("Name", "default");
        Log_ID = sharedPref.getString("login_id", "default");
        loginStatus = sharedPref.getString("Login_Status", "default");


        Log.d(TAG, "name " + name);
        Log.d(TAG, "Log_ID " + Log_ID);
        Log.d(TAG, "loginStatus " + loginStatus);

        android.support.v7.widget.Toolbar mToolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Feedback");
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        progressDialog = new ProgressDialog(Feedback.this);

        Desc = (EditText) findViewById(R.id.Comp_desc);
        button = (Button) findViewById(R.id.comp_Button);

        Desc.setText(predefinedText);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                desc = Desc.getText().toString().trim();


                if (desc.equals(null) || desc.equals("")) {

                    Toast toast = Toast.makeText(Feedback.this, "Please Enter Feedback ", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    Desc.requestFocus();
                    return;
                }

                final JSONObject jsonObject = new JSONObject();
                try {


                    //jsonObject.put("type",desc);


                    jsonObject.put("Log_ID", Log_ID);
                    jsonObject.put("desc", desc);
                    // jsonObject.put("comp_date",S_date);
                    // jsonObject.put("comp_time",S_time);

                    Log.d("msg", "" + desc);

                    Log.d("msg111111111", "Log_ID" + Log_ID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                progressDialog.setMessage("Please Wait");
                progressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String ServerResponse) {

                                Log.d("", ServerResponse);

                                try {
                                    JSONArray jSONArray2 = new JSONArray(ServerResponse);
                                    JSONObject jsonObject2 = jSONArray2.getJSONObject(0);
                                    String loginStatus = jsonObject2.getString("status");


                                    if (loginStatus.equals("1")) {

                                        Toast.makeText(Feedback.this, "Your Feedback Inserted Successfully", Toast.LENGTH_LONG).show();


                                        Desc.setText("");


                                    } else if (loginStatus.equals("0")) {
                                        Toast.makeText(Feedback.this, "Something Went Wrong Please try Again...", Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                progressDialog.dismiss();

                                // progressDialog.dismiss();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {


                                progressDialog.dismiss();
                                Toast.makeText(Feedback.this, "Please Check Network Connection", Toast.LENGTH_LONG).show();


                                // Toast.makeText(Member_Edit_Data.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                            }
                        }) {
                    @Override
                    public String getBodyContentType() {
                        return "text";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return jsonObject.toString().getBytes();
                    }
                };


                RequestQueue requestQueue = Volley.newRequestQueue(Feedback.this);


                requestQueue.add(stringRequest);


            }
        });

    }
}
