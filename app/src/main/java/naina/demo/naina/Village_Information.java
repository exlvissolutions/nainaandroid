package naina.demo.naina;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

public class Village_Information extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_village__information);





        CardView cardView = (CardView) findViewById(R.id.card_view);
        CardView cardView2 = (CardView) findViewById(R.id.card_view2);
        CardView cardView3 = (CardView) findViewById(R.id.card_view3);
        CardView cardView4 = (CardView) findViewById(R.id.card_view4);
        CardView cardView5 = (CardView) findViewById(R.id.card_view5);
        CardView cardView6 = (CardView) findViewById(R.id.card_view6);
        CardView cardView7 = (CardView) findViewById(R.id.card_view7);
        CardView cardView8 = (CardView) findViewById(R.id.card_view8);
        CardView cardView9 = (CardView) findViewById(R.id.card_view9);
        CardView cardView10 = (CardView) findViewById(R.id.card_view10);
        CardView cardView11 = (CardView) findViewById(R.id.card_view11);


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Village_Information.this,Pdf2.class);
                startActivity(intent);
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Village_Information.this,Pdf1.class);
                startActivity(intent);
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Village_Information.this,Pdf3.class);
                startActivity(intent);
            }
        });


        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Village_Information.this,Pdf4.class);
                startActivity(intent);
            }
        });


        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Village_Information.this,Pdf5.class);
                startActivity(intent);
            }
        });


        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Village_Information.this,Pdf6.class);
                startActivity(intent);
            }
        });


        cardView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Village_Information.this,Pdf7.class);
                startActivity(intent);
            }
        });


        cardView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent = new Intent(Village_Information.this,Pdf8.class);
                startActivity(intent);
            }
        });

        cardView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent = new Intent(Village_Information.this,Pdf9.class);
                startActivity(intent);
            }
        });

        cardView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent = new Intent(Village_Information.this,Pdf10.class);
                startActivity(intent);
            }
        });

        cardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent = new Intent(Village_Information.this,Pdf11.class);
                startActivity(intent);
            }
        });


    }


}


