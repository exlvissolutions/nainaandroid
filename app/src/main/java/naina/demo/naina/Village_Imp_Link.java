package naina.demo.naina;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class Village_Imp_Link extends AppCompatActivity {
    ExpandableListView expandableListView;
    ImpLinkListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    private String file_name = "Imp_Link";

    SharedPreferences sharedPref;
    LinkedHashMap<String, List<String>> expandableListDetail;
    private String panel1 = "http://nainabuildersassociation.com/nainapdf/ITPRules_GR/";

    private String panel4 = "http://nainabuildersassociation.com/nainapdf/NAINASanctionedDPandEP/";
    String[] mFilepath = new String[] { panel1+"ITPGR.pdf", panel1+"BGC_ITP_Notification.pdf", panel1+"Final_Notification_ITP_GR_for_sanctioned_RP.pdf", panel1+"ITP_Policy_for_SPA",
            panel4+"Modified_Draft_DCPR",panel4+"NAINA_DP_Excluded_Part_Notification",panel4+"NAINA_DP_sanctioned_Notification"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imp_link_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Important Link");
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        expandableListDetail = ExpandableListDataPannel.getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ImpLinkListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();
//                String pdf_url = "http://nainabuildersassociation.com/nainapdf/ITPGR.pdf";
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url));
//                startActivity(browserIntent);
                Intent intent = new Intent(getApplicationContext() ,WebViewImpLinkActivity.class);
//                intent.putExtra("TITLE", title);
                intent.putExtra("DES", mFilepath[childPosition]);
                startActivity(intent);
                return false;

            }
        });
    }

        @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}
