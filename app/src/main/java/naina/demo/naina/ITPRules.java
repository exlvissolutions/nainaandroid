package naina.demo.naina;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ITPRules extends AppCompatActivity {

    ExpandableListView expandableListView;
    ImpLinkListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    private String file_name = "Eco_Zone";
    SharedPreferences sharedPref;
    LinkedHashMap<String, List<String>> expandableListDetail;
    private String panel2 = "http://nainabuildersassociation.com/nainapdf/ITPRULE/";
    private String panel3 = "http://nainabuildersassociation.com/nainapdf/ITPRULE/";

    String[] mFilepath = new String[] {panel2+"ITP_DP.pdf",
            panel3+"ITP_RP.pdf"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itp_rules);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ITP Rules");
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        expandableListDetail = ITPRulesListDataPannel.getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ImpLinkListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        expandableListTitle.get(groupPosition) + " List Expanded.",
//                        Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        expandableListTitle.get(groupPosition) + " List Collapsed.",
//                        Toast.LENGTH_SHORT).show();

            }
        });


        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
//                Toast.makeText(
//                        getApplicationContext(),
//                        expandableListTitle.get(groupPosition)
//                                + " -> "
//                                + expandableListDetail.get(
//                                expandableListTitle.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT
//                ).show();
//                String pdf_url = "http://nainabuildersassociation.com/nainapdf/ITPGR.pdf";
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url));
//                startActivity(browserIntent);
                Intent intent = new Intent(getApplicationContext() ,WebViewImpLinkActivity.class);
//                intent.putExtra("TITLE", title);
                if(groupPosition == 0)
                intent.putExtra("DES", mFilepath[0]);
                if(groupPosition == 1)
                    intent.putExtra("DES", mFilepath[1]);
                startActivity(intent);
                return false;

            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}
