package naina.demo.naina;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ITPRulesListDataPannel {
    public static LinkedHashMap<String, List<String>> getData() {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();



        List<String> panel2 = new ArrayList<String>();
        panel2.add("DP Area Rules");
//        panel2.add("19_Panvel_Taluka");
//        panel2.add("20_Panvel_taluka");
//        panel2.add("21_Panvel_khalapur_taluka");
//        panel2.add("22_khalapur_taluka");


        List<String> panel3 = new ArrayList<String>();
        panel3.add("RP Area Rules");
//        panel3.add("Sanctioned_Development_Plan_Bazaar_Plot");
//        panel3.add("Sanctioned_Development_Plan_rep__Matheran_Council");
//        panel3.add("Sanctioned_Development_Control_Regulations_Matheran_Council");

        expandableListDetail.put("DP Area Rules", panel2);
        expandableListDetail.put("RP Area Rules", panel3);

        return expandableListDetail;
    }
}
