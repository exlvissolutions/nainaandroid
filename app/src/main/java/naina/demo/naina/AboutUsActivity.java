package naina.demo.naina;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class AboutUsActivity extends Activity {

    private Toolbar aboutUsToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        aboutUsToolbar = findViewById(R.id.aboutUsToolbar);
        aboutUsToolbar.setTitle(getResources().getString(R.string.aboutUsSpelling));
        aboutUsToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        aboutUsToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });





    }//onCreate loses here....



}//AboutUsActivity closes here....
