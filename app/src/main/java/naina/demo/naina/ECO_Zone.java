package naina.demo.naina;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ECO_Zone extends AppCompatActivity {

    ExpandableListView expandableListView;
    ImpLinkListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    private String file_name = "Eco_Zone";
    SharedPreferences sharedPref;
    LinkedHashMap<String, List<String>> expandableListDetail;
    private String panel2 = "http://nainabuildersassociation.com/nainapdf/ZonalMasterPlanforMatheran/";
    private String panel3 = "http://nainabuildersassociation.com/nainapdf/Sub-ZonalMasterPlanforMatheran/";
    private String panel4 = "http://nainabuildersassociation.com/nainapdf/NotificationofESZ/";

    String[] mFilepath = new String[] {panel2+"15_khalapur_taluka.pdf",panel2+"19_Panvel_Taluka.pdf",panel2+"20_Panvel_taluka.pdf",panel2+"21_Panvel_khalapur_taluka.pdf",
            panel2+"22_khalapur_taluka.pdf",panel2+"23_Karjat_Taluka.pdf",panel2+"27_Panvel_Taluka.pdf",panel2+"28_Panvel_Taluka.pdf",panel2+"29_Karjat_Taluka.pdf",
            panel2+"31_Panvel_Taluka.pdf",panel2+"31_Ambernath_Panvel_Talukas.pdf",panel2+"32_Panvel_Taluka_Sanctioned_MAP.pdf",panel2+"33_Ambernath_Taluka.pdf",
            panel2+"34_Ambernath_Karjat_Taluka.pdf",panel2+"index_sheet_showing.pdf",panel2+"List_of_affected_survey.pdf",panel2+"MESZ_Land_use_map.pdf",panel2+"tourism-master-plan-for-matheran.pdf",
            panel2+"ZMP_Report_for_Matheran.pdf",panel2+"Zonal_Master_Plan.pdf"};

    String[] subZonepath = new String[] {panel3+"Sanctioned_Development_Plan_Matheran_Council.pdf",panel3+"Sanctioned_Development_Plan_Bazaar_Plot.pdf",
         panel3+"Sanctioned_Development_Plan_rep__Matheran_Council.pdf",panel3+"Sanctioned_Development_Control_Regulations_Matheran_Council.pdf"};
    String[] nESZFilepath = new String[] {panel4+"Amendment.pdf", panel4+"ESZ.pdf", panel4+"Sanctioned.pdf"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eco__zone);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Eco Sensitive Zone");
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        expandableListDetail = EcoZoneListDataPannel.getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ImpLinkListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();
//                String pdf_url = "http://nainabuildersassociation.com/nainapdf/ITPGR.pdf";
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url));
//                startActivity(browserIntent);
                Intent intent = new Intent(getApplicationContext() ,WebViewImpLinkActivity.class);
//                intent.putExtra("TITLE", title);
                Log.i("AAAAAAAAAAAAA -",Integer.toString(groupPosition));
                Log.i("0000000000000 -",Integer.toString(childPosition));
                if(groupPosition == 0)
                intent.putExtra("DES", mFilepath[childPosition]);

                if(groupPosition == 1)
                    intent.putExtra("DES", subZonepath[childPosition]);

                if(groupPosition == 2)
                    intent.putExtra("DES", nESZFilepath[childPosition]);

                startActivity(intent);
                return false;

            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}
