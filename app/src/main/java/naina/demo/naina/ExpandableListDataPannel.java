package naina.demo.naina;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListDataPannel {
    public static LinkedHashMap<String, List<String>> getData() {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();

        List<String> panel1 = new ArrayList<String>();
        panel1.add("ITPGR");
        panel1.add("BGC_ITP_Notification");
        panel1.add("Final_Notification_ITP_GR_for_sanctioned_RP");
        panel1.add("ITP_Policy_for_SPA");


        List<String> panel4 = new ArrayList<String>();
        panel4.add("Modified_Draft_DCPR");
        panel4.add("NAINA_DP_Excluded_Part_Notification");
        panel4.add("NAINA_DP_sanctioned_Notification");

        expandableListDetail.put("ITP Rules_GR", panel1);
        expandableListDetail.put("NAINA Sanctioned DP AND EP", panel4);
        return expandableListDetail;
    }
}
