package naina.demo.naina

import android.app.Fragment
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


class AssociationAdvertiseFragment : Fragment(), View.OnClickListener {


    //Least priority vvariables goes below....
    val TAG:String = "AssociationAdvertiseFragment".substring(0,23)


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view:View = inflater!!.inflate(R.layout.fragment_association_advertisement, container, false)

        val associationTxtV:TextView = view.findViewById(R.id.associationAdTxtV);
        associationTxtV.setOnClickListener(this)
        return view
    }//onCreateView closes here....


    override fun onClick(view: View?) {
        Log.d(TAG, "view.id == "+view)

        var url:String = "http://nainabuildersassociation.com";
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)

    }//onClick closes here....


}//AssociationAdvertiseFragment closes here....