package naina.demo.naina;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import static naina.demo.naina.R.string.close;
import static naina.demo.naina.R.string.open;

public class Vilage_Image_Front extends AppCompatActivity {
    private DrawerLayout mdrawerLayout;
    private ActionBarDrawerToggle mtoggle;
    String Log_ID, name1;
    private String file_name = "Users_Details";
    private String TAG = "Vilage_Image_Front";
    SharedPreferences sharedPref;
    private String nmmc, mmrda, mahada, pmay, mcqm, thane, kdmc, cidco, msrdc, panvel, maha;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vilage__image__front);

        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);

        name1 = sharedPref.getString("Name", "default");
        nmmc = sharedPref.getString("nmmc", "default");
        mmrda = sharedPref.getString("mmrda", "default");
        mahada = sharedPref.getString("mahada", "default");
        pmay = sharedPref.getString("pmay", "default");
        mcqm = sharedPref.getString("mcqm", "default");
        thane = sharedPref.getString("thane", "default");
        kdmc = sharedPref.getString("kdmc", "default");
        cidco = sharedPref.getString("cidco", "default");
        msrdc = sharedPref.getString("msrdc", "default");
        panvel = sharedPref.getString("panvel", "default");
        maha = sharedPref.getString("maha", "default");

        //Log.d(TAG,"Log_ID "+Log_ID);
        Log.d(TAG, "Log_ID " + name1);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_Vilage_Name);
        mdrawerLayout = (DrawerLayout) findViewById(R.id.navigation_bar_Vilage_Name);

        mtoggle = new ActionBarDrawerToggle(this, mdrawerLayout, open, close);

        mdrawerLayout.addDrawerListener(mtoggle);
        mtoggle.syncState();
        TextView txtProfileName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.textDrawerHeading);
        txtProfileName.setText(name1);

        TextView view = (TextView) navigationView.findViewById(R.id.nav_text);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.exlvis.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mdrawerLayout.closeDrawers();


                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here
                        int id = menuItem.getItemId();
                        if (id == R.id.ProjectEnq) {
                            Intent intent = new Intent(Vilage_Image_Front.this, ProjectEnquiryActivity.class);
                            startActivity(intent);
                        }

                        if (id == R.id.Search_Vilage) {
                            Intent intent = new Intent(Vilage_Image_Front.this, Image_Name_Search.class);
                            startActivity(intent);
                        }

                        if (id == R.id.My_Profile) {
                            Intent intent = new Intent(Vilage_Image_Front.this, Vilage_Profile.class);
                            startActivity(intent);
                        }

                       /* if (id == R.id.My_Log) {
                            Intent intent = new Intent(Vilage_Image_Front.this, My_Log.class);
                            startActivity(intent);
                        }*/

                        if (id == R.id.Information) {
                            Intent intent = new Intent(Vilage_Image_Front.this, Village_Information.class);
                            startActivity(intent);
                        }

                        if (id == R.id.ECO) {
                            Intent intent = new Intent(Vilage_Image_Front.this, ECO_Zone.class);
                            startActivity(intent);
                        }

                        if (id == R.id.ITP) {
                            Intent intent = new Intent(Vilage_Image_Front.this, ITPRules.class);
                            startActivity(intent);
                        }

                        if (id == R.id.Notification) {
                            // Intent intent = new Intent(Vilage_Image_Front.this, Village_Notification.class);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            //startActivity(intent);
//                            String url = "https://www.maharashtra.gov.in/Site/Common/governmentResolutions.aspx";
                            String url = maha;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.ContactUS) {
                            // Intent intent = new Intent(Vilage_Image_Front.this, Village_ContactUs.class);
                            // startActivity(intent);

                            AlertDialog.Builder builder = new AlertDialog.Builder(Vilage_Image_Front.this);
                            builder.setTitle("Contact Us");
//                            builder.setMessage("NAINA Builders Welfare Association\n410/411, Hilton Center, Plot No. 66,\nSector-11,C.B.D. Belapur, Navimumbai,\nph.: 022-40135757/022-41237071 ");
                            builder.setMessage("NAINA Builders Welfare Association\n410/411, Hilton Center, Plot No. 66,\nSector-11,C.B.D. Belapur, Navimumbai 400614 \nEmail : nainabuildersassociation@gmail.com\nMobile : +91 9167213907 \nph.: 022-40135757/022-41237071 ");

                            builder.setCancelable(false);
                            builder.setPositiveButton(" Got It ", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                }
                            });
                            builder.show();

                        }

                        if (id == R.id.Feedback) {
                            Intent intent = new Intent(Vilage_Image_Front.this, Feedback.class);
                            startActivity(intent);
                        }

                        if (id == R.id.Log_Out_vil) {
                            Intent intent = new Intent(Vilage_Image_Front.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }


                        if (id == R.id.Important_Links) {

//                            String url = "https://cidco.maharashtra.gov.in/naina#gsc.tab=0";
                            String url = cidco;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.Imp_Links) {
                            Intent intent = new Intent(Vilage_Image_Front.this, Village_Imp_Link.class);
                            startActivity(intent);


                        }

                       /* if (id == R.id.Exlvis) {
                           // Intent intent = new Intent(Vilage_Image_Front.this, Village_Imp_Link.class);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                           // startActivity(intent);

                            String url = "http://www.exlvis.com";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }*/


                        if (id == R.id.msrdcWebsite) {
//                            String url = "http://www.msrdc.org/1307/Home";
                            String url = msrdc;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.panvelMunCorpWebsite) {
                            String url = panvel;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.nmmcWebsite) {
                            String url = nmmc;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }


                        if (id == R.id.mmrdaWebsite) {
//                            String url = "https://mmrda.maharashtra.gov.in";
                            String url = mmrda;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.mhadaWebsite) {
//                            String url = "https://www.mhada.gov.in/en";
                            String url = mahada;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.pmayWebsite) {
//                            String url = "http://www.pmaymis.gov.in";
                            String url = pmay;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.bmcWebsite) {
//                            String url = "https://portal.mcgm.gov.in/irj/portal/anonymous";
                            String url = mcqm;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.tmcWebsite) {
//                            String url = "https://thanecity.gov.in/marathi/index.php";
                            String url = thane;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.kdmcWebsite) {
                            String url = kdmc;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }



                        if (id == R.id.aboutUs) {
                            Intent intent = new Intent(Vilage_Image_Front.this, AboutUsActivity.class);
                            startActivity(intent);
                        }


                        return true;
                    }


                });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mShare:

                Intent i = new Intent(

                        android.content.Intent.ACTION_SEND);

                i.setType("text/plain");

                i.putExtra(


                        android.content.Intent.EXTRA_TEXT, "Find All NAINA effected Village info \n\n" +
                                "1.  Get All NAINA related notification from CIDCO.\n" +
                                "2. Find all latest infomation by Govt of Maharastra..\n" +
                                "3.Find LAND USE Information of NAINE Area.\n" +

                                "Download Android app.   https://goo.gl/5qm7Yk");

                startActivity(Intent.createChooser(

                        i,

                        "Title of your share dialog"));

                break;
            case R.id.mfacebook:

                String url = "https://facebook.com/";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                break;

            case R.id.mYoutube:
                Intent youtubeIntent = new Intent(Vilage_Image_Front.this, YoutubeLink.class);
                startActivity(youtubeIntent);

        }
        if (mtoggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);

        // return super.onOptionsItemSelected(item);
    }

    public void shareText(View view) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        String shareBodyText = "Your shearing message goes here";
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject/Title");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
        startActivity(Intent.createChooser(intent, "Choose sharing method"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }
}
