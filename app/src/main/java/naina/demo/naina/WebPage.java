package naina.demo.naina;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ProgressBar;

public class WebPage extends AppCompatActivity {
    public static WebView mWebview;
    public static ProgressBar mWebviewProgressBar;
    private final String TAG = "DashboardActivity";
    private String URL = "http://exlvis.com/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_page2);

        mWebview = (WebView) findViewById(R.id.dashboard_webview);
       // mWebviewProgressBar = (ProgressBar) findViewById(R.id.loadWebviewProcess);


        mWebview.loadUrl(this.URL.trim());
    }
}
