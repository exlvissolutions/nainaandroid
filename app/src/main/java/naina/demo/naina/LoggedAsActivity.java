package naina.demo.naina;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class LoggedAsActivity extends AppCompatActivity {
    private Button btContinue, btLogout;
    private TextView tvUserName;
    private String userName,loginStat,userType,soc_ID,type;
    private String file_name = "Users_Details";
    private String TAG = "LoggedAsActivity";
    JSONObject jsonObject = new JSONObject();
    private String requestBody,requestBody1;
    private String checkOTP_URL = "http://contractpro.in/service/Service.svc/FlagCheck";
    int versionCode = 2;
    private SharedPreferences sharedPreferences;
    String versionName = "";
    public static String versionMsg;
    String server_url_versionCheck = "http://www.lagansarai.com/Service.svc/VersionChecks";
    String loadUrl = "";
    private static final int CAMERA_REQUEST = 1888;
    String Log_ID,name,loginStatus;
    SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_as);


        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        // userName = sharedPref.getString("Username","default");
        // loginStat = sharedPref.getString("Login_Status","default");
        // userType = sharedPref.getString("UserType","default");
        name = sharedPref.getString("Name","default");
        Log_ID = sharedPref.getString("login_id","default");
        loginStatus = sharedPref.getString("Login_Status","default");




        Log.d(TAG,"Log_ID "+Log_ID);
        Log.d(TAG,"Log_ID "+name);
        //tvUserName.setText(name);
        initialization();

        appVersionCheck();

        checkPermissions();

        try {
            if (loginStatus.equals("Y")) {
                tvUserName.setText(name);



                btContinue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //TODO:- change activity name
                        Intent intent = new Intent(LoggedAsActivity.this,Vilage_Image_Front.class);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);







                    }
                });

            } else {
                Intent intent = new Intent(LoggedAsActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }catch (Exception e){
            Log.d(TAG,"Excpetion "+e);

           // Intent intent = new Intent(LoggedAsActivity.this, LoginActivity.class);
           // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           // startActivity(intent);
            //finish();
        }




        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.clear();
                editor.commit();

                Intent intent = new Intent(LoggedAsActivity.this,LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });
    }

    private void checkPermissions() {



        int result = ContextCompat.checkSelfPermission(LoggedAsActivity.this, Manifest.permission.CAMERA);


        if (result == PackageManager.PERMISSION_GRANTED){

        }
        else {
            reqPermiscam();
        }

        int result2 = ContextCompat.checkSelfPermission(LoggedAsActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (result2 == PackageManager.PERMISSION_GRANTED){

            //Toast.makeText(getContext(), "Write permission is granted", Toast.LENGTH_SHORT).show();
            Log.d("Inside","checkPermissions 5 ");
        }
        else {
            Log.d("Inside","checkPermissions 6 ");

            reqPermistorage();
        }

    }


    private void reqPermistorage() {

        Log.d("Inside","reqPermistorage 1 ");
        ActivityCompat.requestPermissions(LoggedAsActivity.this,new String []{Manifest.permission.WRITE_EXTERNAL_STORAGE},19);
        int result2 = ContextCompat.checkSelfPermission(LoggedAsActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        Log.d("Inside","reqPermistorage 3 "+result2);

    }
    private void reqPermiscam() {
        // Log.d("Inside","requestPermission 1 ");
        ActivityCompat.requestPermissions(LoggedAsActivity.this,new String []{Manifest.permission.CAMERA},1888);
        // Log.d("Inside","requestPermission 2 ");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 1888){

            if(grantResults.length >0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){


                //Toast.makeText(getContext(), "Camera permission granted", Toast.LENGTH_SHORT).show();
            }
        }
        Log.d("Inside","onRequestPermissionsResult 1 ");
        if(requestCode == 1888){

            if(grantResults.length >0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){

                Log.d("Inside","onRequestPermissionsResult 2 ");
                //Toast.makeText(getContext(), "Camera permission granted", Toast.LENGTH_SHORT).show();
            }
        }

        if(requestCode == 19){

            if(grantResults.length > 0 && grantResults[1]==PackageManager.PERMISSION_GRANTED){

                Log.d("Inside","onRequestPermissionsResult 3 ");
                //Toast.makeText(getContext(), "Write Permission granted", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void checkOTP() {

        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        try {

            jsonObject.put("soc_id", soc_ID);

            Log.d("JSONObject", "" + jsonObject);
            requestBody = jsonObject.toString();
            Log.d("requestBody", "" + requestBody);

        } catch (JSONException e) {
            Log.d("JSON Exception", " " + e);
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, checkOTP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "Login JSON" + response);
                try {
                    JSONArray jSONArray = new JSONArray(response);
                    JSONObject jsonOBJ1 = jSONArray.getJSONObject(0);

                    String status = jsonOBJ1.getString("status");
                    Log.d("Path ","status "+status);

                    String otpFlag = jsonOBJ1.getString("otpflag");
                    Log.d("Path ","otpFlag "+otpFlag);

                    if (otpFlag.equals("") || otpFlag.equals(null)){

                        return;
                    }else {

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("OtpFlag",otpFlag);
                        editor.commit();
                    }

                } catch (JSONException e2) {

                    Log.d("Path "," "+e2);
                    e2.printStackTrace();

                    Toast toast = Toast.makeText(LoggedAsActivity.this, "Network Error. Please try again.", Toast.LENGTH_SHORT);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    Log.e("LoginActivity", "onResponse: Response from Server: " + response);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        MySingleton.getInstance(LoggedAsActivity.this).addToRequest(stringRequest);

    }

    private void initialization() {

        btContinue = (Button) findViewById(R.id.btContinueLA);
        btLogout = (Button) findViewById(R.id.btlogoutLA);
        tvUserName = (TextView) findViewById(R.id.tvUsernameLA);
        sharedPreferences = getSharedPreferences(file_name, Context.MODE_PRIVATE);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    private void appVersionCheck() {
        Log.d("Inside method", "appVersionCheck");
        RetryPolicy policy = new DefaultRetryPolicy(45000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        try {
            jsonObject.put("version_code", versionCode);
            this.requestBody = jsonObject.toString();
            Log.d("requestBody", "" + requestBody);

//            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//            versionName = packageInfo.versionName;
//            versionCode = packageInfo.versionCode;
//            Log.d("Version_name", " " + versionName);
            Log.d("Version_code", " " + versionCode);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//            Log.d("Version name", "Exception " + e);
        } catch (JSONException e) {


        }
        StringRequest stringRequest1 = new StringRequest(1, server_url_versionCheck, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.d("Response from server", "" + response);
                try {
                    versionMsg = new JSONArray(response).getJSONObject(0).getString("Message");
                    int message = Integer.parseInt(versionMsg);
                    String url =  new JSONArray(response).getJSONObject(0).getString("Content");

                    String nmmc =  new JSONArray(response).getJSONObject(0).getString("nmmc").trim();
                    String mmrda =  new JSONArray(response).getJSONObject(0).getString("mmrda").trim();
                    String mahada =  new JSONArray(response).getJSONObject(0).getString("mahada").trim();
                    String pmay =  new JSONArray(response).getJSONObject(0).getString("pmay").trim();
                    String mcqm =  new JSONArray(response).getJSONObject(0).getString("mcqm").trim();
                    String thane =  new JSONArray(response).getJSONObject(0).getString("thane").trim();
                    String kdmc =  new JSONArray(response).getJSONObject(0).getString("kdmc").trim();
                    String cidco =  new JSONArray(response).getJSONObject(0).getString("cidco").trim();
                    String msrdc =  new JSONArray(response).getJSONObject(0).getString("msrdc").trim();
                    String panvel =  new JSONArray(response).getJSONObject(0).getString("panvel").trim();
                    String maha =  new JSONArray(response).getJSONObject(0).getString("maha").trim();
                    Log.d("535345435", "URLerwerwer "+url);
                    loadUrl = url.trim();
                    Log.d("535345435", "URL "+loadUrl);

                    Log.d("Response in JSONFormat", " " + message);

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("nmmc",nmmc);
                    editor.putString("mmrda",mmrda);
                    editor.putString("mahada",mahada);
                    editor.putString("pmay",pmay);
                    editor.putString("mcqm",mcqm);
                    editor.putString("thane",thane);
                    editor.putString("kdmc",kdmc);
                    editor.putString("cidco",cidco);
                    editor.putString("msrdc",msrdc);
                    editor.putString("panvel",panvel);
                    editor.putString("maha",maha);
                    editor.commit();


                    if (message > versionCode) {
                        Log.d("Response in JSONFormat", "Inside IF ");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoggedAsActivity.this);
                        builder1.setTitle("New Update Available");
                        builder1.setMessage("Updated version of Society Visitors app is available.");
                        builder1.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                loadUrl(loadUrl);
                                Log.d("535345435", "URL "+loadUrl);
                            }
                        });
                        builder1.setNegativeButton((CharSequence) "Later", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                finish();
                            }
                        });
                        builder1.setCancelable(false);
                        AlertDialog alertDialog1 = builder1.create();
                        alertDialog1.show();
                        //alertDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                        return;
                    }
                    Log.d("Response in JSONFormat", "Inside else ");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(LoggedAsActivity.this, " Network connection failed. Please try after some time.. ", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                error.printStackTrace();
            }
        }) {
            public byte[] getBody() throws AuthFailureError {
                byte[] bArr = null;
                try {
                    if (requestBody1 != null) {
                        bArr = requestBody1.getBytes("utf-8");
                    }
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody1, "utf-8");
                }
                return bArr;
            }
        };
        stringRequest1.setRetryPolicy(policy);
        try {
            Log.d("String request", "" + stringRequest1);
        } catch (Exception e2) {
        }
        MySingleton.getInstance(this).addToRequest(stringRequest1);
    }

    private void loadUrl(String url) {
        String loadUrl = url;
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(loadUrl));
        startActivity(Intent.createChooser(intent, "Select browser"));
    }

}
