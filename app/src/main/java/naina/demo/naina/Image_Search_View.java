package naina.demo.naina;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.senab.photoview.PhotoViewAttacher;

import static naina.demo.naina.BuildConfig.DEBUG;
import static naina.demo.naina.Image_Name_Search.EXTRA_Vilage_ID;
import static naina.demo.naina.Image_Name_Search.EXTRA_Vilage_Name;

public class Image_Search_View extends AppCompatActivity {

    String Image_Search_view = "http://www.lagansarai.com/Service.svc/Village_images";
    String Image_Button_Log = "http://www.lagansarai.com/Service.svc/VillageName_save";
    String Img;
    ProgressDialog progressDialog;
    String JSON_ImageID = "vil_img";
    EditText editText;
    static int count = 1;
    String value;
    AlertDialog alertDialog;

    ImageView imageView;
    SharedPreferences sharedPref;
    private String file_name = "Users_Details";
    private String TAG = "Image_Search_View";
    String Log_ID1;
    String requestBody;
    String Vilage_Id,Vilage_name;
    String Status;
    String  Status1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image__search__view);
        progressDialog = new ProgressDialog(Image_Search_View.this);
        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        Log_ID1 = sharedPref.getString("login_id","default");


        Log.d(TAG,"Log_ID1 "+Log_ID1);



        Intent intent = getIntent();
        Vilage_Id = intent.getStringExtra(EXTRA_Vilage_ID);
        Vilage_name = intent.getStringExtra(EXTRA_Vilage_Name);

        final TextView textView = (TextView) findViewById(R.id.text_ID);
        editText= (EditText) findViewById(R.id.show_Text);

        imageView = (ImageView) findViewById(R.id.Search_imageView);


//        Log.d("Counter", String.valueOf(count));

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.d("Counter", "hhhi");

                PhotoViewAttacher viewAttacher = new PhotoViewAttacher(imageView);
                viewAttacher.setMaximumScale(30);
                viewAttacher.update();
                textView.setText(Vilage_Id);



            }

        });



        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("vil_Id",Vilage_Id);

//            Log.d("Vilage_Id", ""+Vilage_Id);
//            Log.d("jsonObject", ""+jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        progressDialog.setMessage("Please Wait");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Image_Search_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
//                        Log.d("ServerResponse", "111111111111111111"+ServerResponse);
                        try {


                            JSONArray jSONArray = new JSONArray(ServerResponse);
                            int len = jSONArray.length();
//                            Log.d("Path", "len "+len);
                            for (int i=0;i<jSONArray.length();i++) {
                                JSONObject json = jSONArray.getJSONObject(i);


                                Status1 = json.getString("status");

                               // Picasso.with(getApplicationContext())
                                        //.load( Img = json.getString("vil_img"))

                                        //.into(imageView);
//Log.d(TAG, "URL TEST = "+json.getString("vil_img"));
                                Picasso.with(getApplicationContext())
                                        .load(Img = json.getString("vil_img"))
                                        .into(imageView, new Callback() {
                                            @Override
                                            public void onSuccess() {



                                                editText.postDelayed(new Runnable() {
                                                    public void run() {
                                                        //editText.setVisibility(View.VISIBLE);
                                                        final AlertDialog.Builder dDialog = new AlertDialog.Builder(Image_Search_View.this, R.style.AlertDialogStyle);

                                                        LayoutInflater inflater = getLayoutInflater();
                                                        View alertLayout = inflater.inflate(R.layout.dialog, null);
                                                        final EditText input = alertLayout.findViewById(R.id.surveyNoEdtTxt);
                                                        input.setHint("Please Enter Gutt/Part Number");
                                                        input.setInputType(InputType.TYPE_CLASS_NUMBER);
                                                        input.setFilters(new InputFilter[] { new InputFilter.LengthFilter(55) });
                                                        dDialog.setTitle("Require! ");

                                                        dDialog.setMessage("Your Selected Village is :" +Vilage_name);

                                                        dDialog.setIcon(android.R.drawable.btn_star_big_on);

                                                        dDialog.setView(alertLayout);

                                                        try {

                                                            dDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                                                public void onClick(DialogInterface dialog, int whichButton) {

                           /* Intent intent = new Intent(Image_Name_Search.this, Image_Search_View.class);

                            Image_Name_Page clickedItem = image_name_pages.get(position);
                            intent.putExtra(EXTRA_Vilage_ID, clickedItem.getVil_ID());
                            startActivity(intent);
                            Log.d("Image", EXTRA_Vilage_ID);

                            startActivity(intent);*/
                                                                    // value = input.getText().toString();

                                                                    // Button_Log_Function();
                                                                    value = input.getText().toString();

                                                                    if (value.equals(null) || value.equals("")){

                                                                        Toast toast = Toast.makeText(Image_Search_View.this, "Please Enter Village Name", Toast.LENGTH_LONG);
                                                                        toast.setGravity(17, 0, 0);
                                                                        toast.show();
                                                                        input.requestFocus();
                                                                        return;
                                                                    }else {


                                                                        final JSONObject jsonObject1 = new JSONObject();
                                                                        try {
                                                                            jsonObject1.put("vil_name", value);
                                                                            jsonObject1.put("log_id", Log_ID1);
//                                                                            Log.d("value", value);
//                                                                            Log.d("Log_ID1", Log_ID1);
//                                                                            Log.d("jsonObject1", "" + jsonObject1);
                                                                            requestBody = jsonObject1.toString();
//                                                                            Log.d("requestBody", "" + requestBody);
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }


                                                                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Image_Button_Log,
                                                                                new Response.Listener<String>() {
                                                                                    @Override
                                                                                    public void onResponse(String ServerResponse) {
//                                                                                        Log.d("ServerResponse", ServerResponse);
                                                                                        try {


                                                                                            JSONArray jSONArray = new JSONArray(ServerResponse);
                                                                                            int len = jSONArray.length();
//                                                                                            Log.d("Path", "len " + len);
                                                                                            for (int i = 0; i < jSONArray.length(); i++) {
                                                                                                JSONObject json = jSONArray.getJSONObject(i);

                                                                                                Status = json.getString("status");
                                                                                                //String loginStatus = jsonObject1.getString("Status");
                                                                                                Log.d("loginStatus", "" + Status);



                                                                                            }



                                                                                        } catch (JSONException e) {
                                                                                            e.printStackTrace();
                                                                                        }
                                                                                        if (Status.equals("1")) {
                                                                                            showProgress(false);

                                                                                            Toast toast = Toast.makeText(Image_Search_View.this, "Thank you for Feedback", Toast.LENGTH_SHORT);
                                                                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                                                                            toast.show();


                                                                                            return;
                                                                                        }
                                                                                        if (Status.equals("0")) {
                                                                                            showProgress(false);
                                                                                            Toast toast = Toast.makeText(Image_Search_View.this, "Please try again", Toast.LENGTH_SHORT);
                                                                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                                                                            toast.show();
                                                                                            return;
                                                                                        }
                                                                                        showProgress(true);

                                                                                        progressDialog.dismiss();
                                                                                    }


                                                                                },
                                                                                new Response.ErrorListener() {
                                                                                    @Override
                                                                                    public void onErrorResponse(VolleyError volleyError) {

                                                                                        progressDialog.dismiss();

                                                                                        Toast.makeText(Image_Search_View.this, "Something Wrong Please try Again... ", Toast.LENGTH_LONG).show();
                                                                                        showProgress(false);
                                                                                    }
                                                                                }) {
                                                                            @Override
                                                                            public String getBodyContentType() {
                                                                                return "text";
                                                                            }

                                                                            @Override
                                                                            public byte[] getBody() throws AuthFailureError {
                                                                                return jsonObject1.toString().getBytes();
                                                                            }
                                                                        };




                                                                        RequestQueue requestQueue = Volley.newRequestQueue(Image_Search_View.this);


                                                                        requestQueue.add(stringRequest);
                                                                    }
                                                                }

                                                            });
                                                            dDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                                    // Intent intent = new Intent(Image_Name_Search.this, Image_Name_Search.class);
                                                                    // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                    //startActivity(intent);
                                                                    //value = input.getText().toString();

                                                                    // Toast.makeText(Image_Search_View.this,"Your Click [Cancel] value = " + value ,

                                                                    //  Toast.LENGTH_LONG).show();

                                                                }

                                                            });
                                                            alertDialog = dDialog.create();
                                                            alertDialog.show();
                                                            alertDialog.getWindow().setLayout(650, 500);


                                                        }catch (Exception e){
                                                            //Toast.makeText(Image_Search_View.this,"Network Error Please try again... ", Toast.LENGTH_LONG).show();
                                                            //showProgress(false);
                                                        }
                                                    }
                                                }, 30000);

                                            }

                                            @Override
                                            public void onError() {
                                                Toast.makeText(Image_Search_View.this,"Image not Found ", Toast.LENGTH_LONG).show();
                                                showProgress(false);

                                            }
                                        });

                            }

                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                        if (Status1.equals("1")) {
                            showProgress(false);

                           // progressDialog.setVisibility(View.GONE);

                            return;
                        }
                        Log.d("imageView1111111111111", ""+imageView);
                       // progressDialog.setVisibility(View.GONE);

                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {



                        Toast.makeText(Image_Search_View.this,"Network Error Please try Again ", Toast.LENGTH_LONG).show();
                        showProgress(false);


                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "text";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };




        RequestQueue requestQueue = Volley.newRequestQueue(Image_Search_View.this);


        requestQueue.add(stringRequest);
        //progressDialog.setVisibility(View.GONE);
    }





    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");

            if (imageView == null) {

                return;

            } else {
                Log.d("Inside Showprogress", "Elsepart");


               // progressDialog.setVisibility(View.GONE);


            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

switch (item.getItemId()){

    case R.id.test:


        Intent  intent = new Intent(Image_Search_View.this,Village_Information.class);
        startActivity(intent);

        break;

   /* case R.id.test2:

        Intent intent1 = new Intent(Image_Search_View.this,Pdf2.class);
        startActivity(intent1);

        break;

    case R.id.test3:

        Intent  intent2 = new Intent(Image_Search_View.this,Pdf3.class);
        startActivity(intent2);

        break;

    case R.id.test4:

        Intent  intent3 = new Intent(Image_Search_View.this,Pdf4.class);
        startActivity(intent3);

        break;

    case R.id.test5:

        Intent  intent4 = new Intent(Image_Search_View.this,Pdf5.class);
        startActivity(intent4);

        break;

    case R.id.test6:

        Intent  intent5 = new Intent(Image_Search_View.this,Pdf6.class);
        startActivity(intent5);

        break;

    case R.id.test7:

        Intent  intent6 = new Intent(Image_Search_View.this,Pdf7.class);
        startActivity(intent6);

        break;*/

        default:
}


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.setting_menu, menu);


    /* for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            spanString.setSpan(new ForegroundColorSpan(Color.RED), 0,     spanString.length(), 0); //fix the color to white
            item.setTitle(spanString);
        }*/

        return super.onCreateOptionsMenu(menu);
    }

}
