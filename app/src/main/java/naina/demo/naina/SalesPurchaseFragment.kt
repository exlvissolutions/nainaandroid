package naina.demo.naina

import android.app.Fragment
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.TextView


class SalesPurchaseFragment : Fragment(), View.OnClickListener {


    //Least priority vvariables goes below....
//    val TAG:String = "SalesPurchaseFragment".substring(0,23)


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view:View = inflater!!.inflate(R.layout.fragment_sales_purchase, container, false)

        val associationTxtV:TextView = view.findViewById(R.id.associationAdTxtV);
        associationTxtV.setOnClickListener(this)
        associationTxtV.blink()

        return view
    }//onCreateView closes here....
    fun View.blink(
            times: Int = Animation.INFINITE,
            duration: Long = 50L,
            offset: Long = 250L,
            minAlpha: Float = 0.0f,
            maxAlpha: Float = 1.0f,
            repeatMode: Int = Animation.REVERSE
    ) {
        startAnimation(AlphaAnimation(minAlpha, maxAlpha).also {
            it.duration = duration
            it.startOffset = offset
            it.repeatMode = repeatMode
            it.repeatCount = times
        })
    }


    override fun onClick(view: View?) {
//        Log.d(TAG, "view.id == "+view)

       var intent = Intent(context, ProjectEnquiryActivity::class.java)
        startActivity(intent)

//        var url:String = "http://nainabuildersassociation.com";
//        val i = Intent(Intent.ACTION_VIEW)
//        i.data = Uri.parse(url)
//        startActivity(i)

    }//onClick closes here....


}//AssociationAdvertiseFragment closes here....