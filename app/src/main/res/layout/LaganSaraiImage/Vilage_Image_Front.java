package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import static com.exlvis.LaganSaraiImage.R.string.close;
import static com.exlvis.LaganSaraiImage.R.string.open;

public class Vilage_Image_Front extends AppCompatActivity {
    private DrawerLayout mdrawerLayout;
    private ActionBarDrawerToggle mtoggle;
    String Log_ID,name1;
    private String file_name = "Users_Details";
    private String TAG = "Vilage_Image_Front";
    SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vilage__image__front);


        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);

        name1 = sharedPref.getString("Name","default");

        //Log.d(TAG,"Log_ID "+Log_ID);
        Log.d(TAG,"Log_ID "+name1);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_Vilage_Name);
        mdrawerLayout = (DrawerLayout) findViewById(R.id.navigation_bar_Vilage_Name);

        mtoggle = new ActionBarDrawerToggle(this, mdrawerLayout, open, close);

        mdrawerLayout.addDrawerListener(mtoggle);
        mtoggle.syncState();
        TextView txtProfileName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.textDrawerHeading);
        txtProfileName.setText(name1);

TextView view = (TextView) navigationView.findViewById(R.id.nav_text);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.exlvis.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
           getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem ) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mdrawerLayout.closeDrawers();



                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here
                        int id = menuItem.getItemId();


                        if (id == R.id.Search_Vilage) {
                            Intent intent = new Intent(com.exlvis.LaganSaraiImage.Vilage_Image_Front.this, com.exlvis.LaganSaraiImage.Image_Name_Search.class);
                            startActivity(intent);
                        }

                        if (id == R.id.My_Profile) {
                            Intent intent = new Intent(com.exlvis.LaganSaraiImage.Vilage_Image_Front.this, com.exlvis.LaganSaraiImage.Vilage_Profile.class);
                            startActivity(intent);
                        }

                       /* if (id == R.id.My_Log) {
                            Intent intent = new Intent(Vilage_Image_Front.this, My_Log.class);
                            startActivity(intent);
                        }*/

                        if (id == R.id.Information) {
                            Intent intent = new Intent(com.exlvis.LaganSaraiImage.Vilage_Image_Front.this, com.exlvis.LaganSaraiImage.Village_Information.class);
                            startActivity(intent);
                        }

                        if (id == R.id.Notification) {
                           // Intent intent = new Intent(Vilage_Image_Front.this, Village_Notification.class);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            //startActivity(intent);


                            String url = "https://www.maharashtra.gov.in/Site/Common/governmentResolutions.aspx";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                        if (id == R.id.ContactUS) {
                            Intent intent = new Intent(com.exlvis.LaganSaraiImage.Vilage_Image_Front.this, com.exlvis.LaganSaraiImage.Village_ContactUs.class);
                            startActivity(intent);
                        }


                        if (id == R.id.Log_Out_vil) {
                            Intent intent = new Intent(com.exlvis.LaganSaraiImage.Vilage_Image_Front.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }



                        if (id == R.id.Important_Links) {
                           // Intent intent = new Intent(Vilage_Image_Front.this, Village_Imp_Link.class);
                           // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            //startActivity(intent);

                            String url = "https://cidco.maharashtra.gov.in/naina";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }

                       /* if (id == R.id.Exlvis) {
                           // Intent intent = new Intent(Vilage_Image_Front.this, Village_Imp_Link.class);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                           // startActivity(intent);

                            String url = "http://www.exlvis.com";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }*/

                        return true;
                    }


                });



        }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mShare:

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = "Check it out. Your message goes here";
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Shearing Option"));
                return true;

        }
            if (mtoggle.onOptionsItemSelected(item)) {
               return true;
            }

            return super.onOptionsItemSelected(item);

       // return super.onOptionsItemSelected(item);
    }
    public void shareText(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String shareBodyText = "Your shearing message goes here";
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject/Title");
        intent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
        startActivity(Intent.createChooser(intent, "Choose sharing method"));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
