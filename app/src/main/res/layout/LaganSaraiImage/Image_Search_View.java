package com.exlvis.LaganSaraiImage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.senab.photoview.PhotoViewAttacher;

import static com.exlvis.LaganSaraiImage.Image_Name_Search.EXTRA_Vilage_ID;

public class Image_Search_View extends AppCompatActivity {

    String Image_Search_view = "http://www.lagansarai.com/Service.svc/Village_images";
    String Image_Button_Log = "http://www.lagansarai.com/Service.svc/VillageName_save";
    String Img;
    ProgressDialog progressDialog;
    String JSON_ImageID = "vil_img";
    EditText editText;
    static int count = 1;
    String value;
    ImageView imageView;
    SharedPreferences sharedPref;
    private String file_name = "Users_Details";
    private String TAG = "Image_Search_View";
    String Log_ID1;
    String requestBody;
    String Vilage_Id;
    String Status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image__search__view);

        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        Log_ID1 = sharedPref.getString("login_id","default");


        Log.d(TAG,"Log_ID1 "+Log_ID1);

        progressDialog = new ProgressDialog(com.exlvis.LaganSaraiImage.Image_Search_View.this);
        Intent intent = getIntent();
         Vilage_Id = intent.getStringExtra(EXTRA_Vilage_ID);

        final TextView textView = (TextView) findViewById(R.id.text_ID);
        editText= (EditText) findViewById(R.id.show_Text);

        imageView = (ImageView) findViewById(R.id.Search_imageView);

        editText.postDelayed(new Runnable() {
            public void run() {
                //editText.setVisibility(View.VISIBLE);
                final AlertDialog.Builder dDialog = new AlertDialog.Builder(com.exlvis.LaganSaraiImage.Image_Search_View.this);

                final EditText input = new EditText(com.exlvis.LaganSaraiImage.Image_Search_View.this);
                input.setHint("Please Enter Village Name");

                dDialog.setTitle("Require! ");

                dDialog.setMessage("Please Enter Village name :");

                dDialog.setIcon(android.R.drawable.btn_star_big_on);

                dDialog.setView(input);

                try {

                    dDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {

                           /* Intent intent = new Intent(Image_Name_Search.this, Image_Search_View.class);

                            Image_Name_Page clickedItem = image_name_pages.get(position);
                            intent.putExtra(EXTRA_Vilage_ID, clickedItem.getVil_ID());
                            startActivity(intent);
                            Log.d("Image", EXTRA_Vilage_ID);

                            startActivity(intent);*/
                            // value = input.getText().toString();

                            // Button_Log_Function();
                            value = input.getText().toString();

                            if (value.equals(null) || value.equals("")){

                                Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Image_Search_View.this, "Please Enter Village Name", Toast.LENGTH_LONG);
                                toast.setGravity(17, 0, 0);
                                toast.show();
                                input.requestFocus();
                                return;
                            }else {


                                final JSONObject jsonObject1 = new JSONObject();
                                try {
                                    jsonObject1.put("vil_name", value);
                                    jsonObject1.put("log_id", Log_ID1);
                                    Log.d("value", value);
                                    Log.d("Log_ID1", Log_ID1);
                                    Log.d("jsonObject1", "" + jsonObject1);
                                    requestBody = jsonObject1.toString();
                                    Log.d("requestBody", "" + requestBody);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                StringRequest stringRequest = new StringRequest(Request.Method.POST, Image_Button_Log,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String ServerResponse) {
                                                Log.d("ServerResponse", ServerResponse);
                                                try {


                                                    JSONArray jSONArray = new JSONArray(ServerResponse);
                                                    int len = jSONArray.length();
                                                    Log.d("Path", "len " + len);
                                                    for (int i = 0; i < jSONArray.length(); i++) {
                                                        JSONObject json = jSONArray.getJSONObject(i);

                                                        Status = json.getString("status");
                                                        //String loginStatus = jsonObject1.getString("Status");
                                                        Log.d("loginStatus", "" + Status);



                                                    }



                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                if (Status.equals("1")) {
                                                    showProgress(false);

                                                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Image_Search_View.this, "Village Name Register Successfully ", Toast.LENGTH_SHORT);
                                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                                    toast.show();


                                                    return;
                                                }
                                                if (Status.equals("0")) {
                                                    showProgress(false);
                                                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Image_Search_View.this, "Please try again", Toast.LENGTH_SHORT);
                                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                                    toast.show();
                                                    return;
                                                }
                                                showProgress(true);

                                                progressDialog.dismiss();
                                            }


                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {


                                                progressDialog.dismiss();
                                                Toast.makeText(com.exlvis.LaganSaraiImage.Image_Search_View.this, "Something Wrong Please try Again... ", Toast.LENGTH_LONG).show();
                                                showProgress(false);
                                            }
                                        }) {
                                    @Override
                                    public String getBodyContentType() {
                                        return "text";
                                    }

                                    @Override
                                    public byte[] getBody() throws AuthFailureError {
                                        return jsonObject1.toString().getBytes();
                                    }
                                };




                                RequestQueue requestQueue = Volley.newRequestQueue(com.exlvis.LaganSaraiImage.Image_Search_View.this);


                                requestQueue.add(stringRequest);
                            }
                        }

                    });
                    dDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Intent intent = new Intent(Image_Name_Search.this, Image_Name_Search.class);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            //startActivity(intent);
                            //value = input.getText().toString();

                            // Toast.makeText(Image_Search_View.this,"Your Click [Cancel] value = " + value ,

                            //  Toast.LENGTH_LONG).show();

                        }

                    });
                    dDialog.show();

                }catch (Exception e){
                    Toast.makeText(com.exlvis.LaganSaraiImage.Image_Search_View.this,"Network Error Please try again... ", Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
            }
        }, 3000);

        Log.d("Counter", String.valueOf(count));

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Counter", "hhhi");

                PhotoViewAttacher viewAttacher = new PhotoViewAttacher(imageView);
                viewAttacher.setMaximumScale(30);
                viewAttacher.update();
                textView.setText(Vilage_Id);



            }

        });

        progressDialog.setMessage("Please Wait");
        progressDialog.show();
        showProgress(true);

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("vil_Id",Vilage_Id);

            Log.d("Vilage_Id", ""+Vilage_Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Image_Search_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        progressDialog.setMessage("Please Wait");
                        progressDialog.show();
                        try {


                            JSONArray jSONArray = new JSONArray(ServerResponse);
                            int len = jSONArray.length();
                            Log.d("Path", "len "+len);
                            for (int i=0;i<jSONArray.length();i++) {
                                JSONObject json = jSONArray.getJSONObject(i);




                                Picasso.with(getApplicationContext())
                                        .load( Img = json.getString("vil_img"))

                                        .into(imageView);


                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("imageView1111111111111", ""+imageView);

                        progressDialog.dismiss();
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {



                        Toast.makeText(com.exlvis.LaganSaraiImage.Image_Search_View.this,"Network Error Please try Again ", Toast.LENGTH_LONG).show();
                        showProgress(false);

                        progressDialog.dismiss();
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "text";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };




        RequestQueue requestQueue = Volley.newRequestQueue(com.exlvis.LaganSaraiImage.Image_Search_View.this);


        requestQueue.add(stringRequest);

    }





    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");

            return;
        }
        Log.d("Inside Showprogress", "Elsepart");
    }
}
