package com.exlvis.LaganSaraiImage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class Pdf7 extends AppCompatActivity {
    PDFView pdfView7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf7);

        pdfView7 = (PDFView) findViewById(R.id.pdfView7);

        pdfView7.fromAsset("N7.pdf").load();
    }
}
