package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class Village_Noti_Adapter extends RecyclerView.Adapter<com.exlvis.LaganSaraiImage.Village_Noti_Adapter.ViewHolder> {

    Context context;

    ArrayList<com.exlvis.LaganSaraiImage.Village_Noti_List> village_noti_lists;
private OnItemClickListener onItemClickListener;


public interface OnItemClickListener {

        void button1(int position);

    }

    public void setOnItemClickListener(com.exlvis.LaganSaraiImage.Village_Noti_Adapter.OnItemClickListener listener) {
        onItemClickListener =  listener;
    }
    public Village_Noti_Adapter(ArrayList<com.exlvis.LaganSaraiImage.Village_Noti_List> village_noti_lists1, Context context) {

        super();

        this.village_noti_lists = village_noti_lists1;
        this.context = context;
    }
    @Override
    public com.exlvis.LaganSaraiImage.Village_Noti_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.village_noti_adapter, parent, false);

        ViewHolder holder = new ViewHolder(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(com.exlvis.LaganSaraiImage.Village_Noti_Adapter.ViewHolder holder, int position) {
        final com.exlvis.LaganSaraiImage.Village_Noti_List vendor_list = village_noti_lists.get(position);
        holder.Noti_Title.setText(vendor_list.getNoti_Title());
        holder.Noti_Desc.setText(vendor_list.getNoti_desc());
    }

    @Override
    public int getItemCount() {
        return village_noti_lists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView Noti_Title;
        public TextView Noti_Desc;
        public Button ViewButton;
        public ViewHolder(View itemView) {
            super(itemView);

            Noti_Title = (TextView) itemView.findViewById(R.id.noti_title);
            Noti_Desc = (TextView) itemView.findViewById(R.id.noti_Desc);
            ViewButton = (Button) itemView.findViewById(R.id.Navi_button);


            ViewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            onItemClickListener.button1(position);
                        }
                    }
                }
            });
        }
    }
}
