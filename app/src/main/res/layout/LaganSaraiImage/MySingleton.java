package com.exlvis.LaganSaraiImage;

/**
 * Created by bhushandc on 8/11/17.
 */


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MySingleton {

    private static Context ctx;
    private static com.exlvis.LaganSaraiImage.MySingleton mInstance;
    private RequestQueue requestQueue;

    private MySingleton(Context context) {
        ctx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized com.exlvis.LaganSaraiImage.MySingleton getInstance(Context context) {

        if (mInstance == null){
            mInstance = new com.exlvis.LaganSaraiImage.MySingleton(context);
        }

        return mInstance;
    }

    public RequestQueue getRequestQueue() {

        if (requestQueue == null){

            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T>void addToRequest(Request request){

        requestQueue.add(request);
    }

}

