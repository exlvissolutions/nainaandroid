package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.ProgressBar;

//import android.webkit.WebChromeClient;
//import android.webkit.WebViewClient;


public class WebViewActivity extends AppCompatActivity {

    public static WebView mWebview;
    public static ProgressBar mWebviewProgressBar;
    private final String TAG = "DashboardActivity";
    private String LoadURL = "http://www.societyvisitors.com/Home/sample?";
    String call_from;
    //DataBaseHelper dbhelper;
    String password;
    String userName;
    SharedPreferences sharedPref;
    private String file_name = "Users_Details";

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_web_view);
        initializations();
        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= 21) {
            cookieManager.removeAllCookies(new ValueCallback<Boolean>() {
                public void onReceiveValue(Boolean aBoolean) {
                    Log.d("DashboardActivity", "Cookie removed: " + aBoolean);
                }
            });
        } else {
            cookieManager.removeAllCookie();
        }

        call_from = getIntent().getStringExtra("Call_From");

        try {
            //TODO :  Take values from shared preferences

            sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
            userName = sharedPref.getString("Username","default");
            userName = Base64.encodeToString(userName.getBytes(),Base64.DEFAULT);
            Log.d("WebViewAct"," "+userName);
            password = sharedPref.getString("Password","default");
            password = Base64.encodeToString(password.getBytes(),Base64.DEFAULT);
            Log.d("WebViewAct"," "+password);

        } catch (Exception e2) {
            Log.d("getUserDetail()", "Exception " + e2);
        }

        try {

            if (call_from.equals("UTL")){

                LoadURL = "http://societyvisitors.com/Utility/UTL";
                Log.d("WebViewAct"," "+LoadURL);
                mWebview.loadUrl(LoadURL.trim());
                mWebview.getSettings().setJavaScriptEnabled(true);

            }else if (call_from.equals("Chat")){

                LoadURL = "http://societyvisitors.com/Utility/chat";
                Log.d("WebViewAct"," "+LoadURL);
                mWebview.loadUrl(LoadURL.trim());
                mWebview.getSettings().setJavaScriptEnabled(true);

            }
            else {

                LoadURL = LoadURL + "user=" + userName.trim() + "&pass=" + password.trim();
                Log.d("WebViewAct", " " + LoadURL);
                mWebview.loadUrl(LoadURL.trim());
                mWebview.getSettings().setJavaScriptEnabled(true);
            }

        } catch (Exception e22) {

            Log.d("LoadingURL ", "Exception " + e22);

        }
    }

    private void initializations() {
        mWebview = (WebView) findViewById(R.id.WebViewSociety);
        mWebviewProgressBar = (ProgressBar) findViewById(R.id.webProgressBar);

        generateEvents();
    }

    private void generateEvents() {

        mWebview.setWebViewClient(new com.exlvis.LaganSaraiImage.WebviewClient(this,this));
        mWebview.setWebChromeClient(new WebViewChromeClient(this,this));

    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case 4:
                    //mWebview.goBack();
                    if (mWebview.canGoBack()) {
                        mWebview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
