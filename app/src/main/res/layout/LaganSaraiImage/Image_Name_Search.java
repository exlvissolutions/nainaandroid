package com.exlvis.LaganSaraiImage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Image_Name_Search extends AppCompatActivity implements Image_Name_Search_Adapter.OnItemClickListener {
    String HttpUrl = "http://www.lagansarai.com/Service.svc/Village_Load";
    String Image_Button_Log = "http://www.lagansarai.com/Service.svc/VillageName_save";
    private String file_name = "Users_Details";
    private String TAG = "Image_Name_Search";
    private Image_Name_Search_Adapter image_name_search_adapter;
    ArrayList<Image_Name_Page> image_name_pages;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    public static final String EXTRA_Vilage_ID = "vil_id";
    String JSON_Vilage_Name= "vil_name";
    String JSON_Vilage_ID= "vil_id";
    EditText editText11;
    String value;
    String Log_ID1;
    String requestBody;
    String Status;
    String Vilage_Id;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image__name__search);

        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        Log_ID1 = sharedPref.getString("login_id","default");


        Log.d(TAG,"Log_ID1 "+Log_ID1);

        progressDialog = new ProgressDialog(com.exlvis.LaganSaraiImage.Image_Name_Search.this);
        EditText editText = (EditText) findViewById(R.id.Search_bar_Vilage);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
        image_name_pages = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.Vilage_Name_Search_Recycleview);
        recyclerView.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);

        editText11= (EditText) findViewById(R.id.show_Text11);




        progressDialog.setMessage("Please Wait");
        progressDialog.show();
        showProgress(true);
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("","");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        Log.d("", ServerResponse);



                        try {


                            JSONArray jSONArray = new JSONArray(ServerResponse);
                            int len = jSONArray.length();
                            Log.d("Path", "len "+len);
                            for (int i=0;i<jSONArray.length();i++) {
                                JSONObject json = jSONArray.getJSONObject(i);


                                Image_Name_Page namePage = new Image_Name_Page();

                                namePage.setVilage_name(json.getString(JSON_Vilage_Name));
                                namePage.setVil_ID(json.getString(JSON_Vilage_ID));

                                image_name_pages.add(namePage);


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        image_name_search_adapter = new Image_Name_Search_Adapter((ArrayList<Image_Name_Page>) image_name_pages, com.exlvis.LaganSaraiImage.Image_Name_Search.this);

                        image_name_search_adapter.setOnItemClickListener(com.exlvis.LaganSaraiImage.Image_Name_Search.this);




                        Log.d("",ServerResponse);
                        recyclerView.setAdapter(image_name_search_adapter);


                        progressDialog.dismiss();
                    }



                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {


                        progressDialog.dismiss();


                        Toast.makeText(com.exlvis.LaganSaraiImage.Image_Name_Search.this,"Network ErrorPlease Try again... ", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "text";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };




        RequestQueue requestQueue = Volley.newRequestQueue(com.exlvis.LaganSaraiImage.Image_Name_Search.this);


        requestQueue.add(stringRequest);

    }

    private void filter(String s) {

        ArrayList<Image_Name_Page> filteredList = new ArrayList<>();

        for (Image_Name_Page item : image_name_pages) {

            if (item.getVilage_name().toLowerCase().contains(s.toLowerCase())) {
                filteredList.add(item);
            }

        }

        image_name_search_adapter.filterList(filteredList);
    }

    @Override
    public void onViewButton(final int position) {


        editText11.postDelayed(new Runnable() {
            public void run() {
                //editText.setVisibility(View.VISIBLE);
                final AlertDialog.Builder dDialog = new AlertDialog.Builder(com.exlvis.LaganSaraiImage.Image_Name_Search.this);

                final EditText input = new EditText(com.exlvis.LaganSaraiImage.Image_Name_Search.this);
                input.setHint("Please Enter Village Name");

                dDialog.setTitle("Require! ");

                dDialog.setMessage("Please Enter Village name :");

                dDialog.setIcon(android.R.drawable.btn_star_big_on);

                dDialog.setView(input);

                try {

                    dDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {

                           /* Intent intent = new Intent(Image_Name_Search.this, Image_Search_View.class);

                            Image_Name_Page clickedItem = image_name_pages.get(position);
                            intent.putExtra(EXTRA_Vilage_ID, clickedItem.getVil_ID());
                            startActivity(intent);
                            Log.d("Image", EXTRA_Vilage_ID);

                            startActivity(intent);*/
                            // value = input.getText().toString();

                            // Button_Log_Function();
                            value = input.getText().toString();

                            if (value.equals(null) || value.equals("")){

                                Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Image_Name_Search.this, "Please Enter Village Name", Toast.LENGTH_LONG);
                                toast.setGravity(17, 0, 0);
                                toast.show();
                                input.requestFocus();
                                return;
                            }else {


                                final JSONObject jsonObject1 = new JSONObject();
                                try {
                                    jsonObject1.put("vil_name", value);
                                    jsonObject1.put("log_id", Log_ID1);
                                    Log.d("value", value);
                                    Log.d("Log_ID1", Log_ID1);
                                    Log.d("jsonObject1", "" + jsonObject1);
                                    requestBody = jsonObject1.toString();
                                    Log.d("requestBody", "" + requestBody);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                StringRequest stringRequest = new StringRequest(Request.Method.POST, Image_Button_Log,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String ServerResponse) {
                                                Log.d("ServerResponse", ServerResponse);
                                                try {


                                                    JSONArray jSONArray = new JSONArray(ServerResponse);
                                                    int len = jSONArray.length();
                                                    Log.d("Path", "len " + len);
                                                    for (int i = 0; i < jSONArray.length(); i++) {
                                                        JSONObject json = jSONArray.getJSONObject(i);

                                                        Status = json.getString("status");
                                                        //String loginStatus = jsonObject1.getString("Status");
                                                        Log.d("loginStatus", "" + Status);



                                                    }



                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                               if (Status.equals("1")) {
                                                    showProgress(false);

                                                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Image_Name_Search.this, "Village Name Register Successfully ", Toast.LENGTH_SHORT);
                                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                                    toast.show();

                                                    Intent intent11 = new Intent(com.exlvis.LaganSaraiImage.Image_Name_Search.this, Image_Search_View.class);
                                                    Image_Name_Page clickedItem = image_name_pages.get(position);
                                                    intent11.putExtra(EXTRA_Vilage_ID, clickedItem.getVil_ID());
                                                    startActivity(intent11);
                                                    Log.d("Image", EXTRA_Vilage_ID);

                                                    return;
                                                }
                                                if (Status.equals("0")) {
                                                    showProgress(false);
                                                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Image_Name_Search.this, "Please try again", Toast.LENGTH_SHORT);
                                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                                    toast.show();
                                                    return;
                                                }
                                                showProgress(true);

                                                progressDialog.dismiss();
                                            }


                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {


                                                progressDialog.dismiss();
                                                Toast.makeText(com.exlvis.LaganSaraiImage.Image_Name_Search.this, "Something Wrong Please try Again... ", Toast.LENGTH_LONG).show();
                                                showProgress(false);
                                            }
                                        }) {
                                    @Override
                                    public String getBodyContentType() {
                                        return "text";
                                    }

                                    @Override
                                    public byte[] getBody() throws AuthFailureError {
                                        return jsonObject1.toString().getBytes();
                                    }
                                };




                                  RequestQueue requestQueue = Volley.newRequestQueue(com.exlvis.LaganSaraiImage.Image_Name_Search.this);


                                  requestQueue.add(stringRequest);
                            }
                        }

                    });
                    dDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Intent intent = new Intent(Image_Name_Search.this, Image_Name_Search.class);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            //startActivity(intent);
                            //value = input.getText().toString();

                            // Toast.makeText(Image_Search_View.this,"Your Click [Cancel] value = " + value ,

                            //  Toast.LENGTH_LONG).show();

                        }

                    });
                    dDialog.show();

                }catch (Exception e){
                    Toast.makeText(com.exlvis.LaganSaraiImage.Image_Name_Search.this,"Network Error Please try again... ", Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
            }
        }, 100);
        // view.setVisibility(View.INVISIBLE|View.VISIBLE|View.GONE);




       /* Intent detailIntent = new Intent(this, Image_Search_View.class);
        Image_Name_Page clickedItem = image_name_pages.get(position);
        detailIntent.putExtra(EXTRA_Vilage_ID, clickedItem.getVil_ID());
        startActivity(detailIntent);
        Log.d("Image", EXTRA_Vilage_ID);*/

    }

    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");

            return;
        }
        Log.d("Inside Showprogress", "Elsepart");
    }


}
