package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class My_Log_Adapter extends RecyclerView.Adapter<com.exlvis.LaganSaraiImage.My_Log_Adapter.ViewHolder> {

    Context context;

    ArrayList<My_Log_List> my_log_lists;

    public My_Log_Adapter(ArrayList<My_Log_List> my_log_lists1, Context context) {

        super();

        this.my_log_lists = my_log_lists1;
        this.context = context;
    }

        @Override
    public com.exlvis.LaganSaraiImage.My_Log_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_log_details, parent, false);
    ViewHolder viewHolder = new ViewHolder(v);
    return viewHolder;
    }

    @Override
    public void onBindViewHolder(com.exlvis.LaganSaraiImage.My_Log_Adapter.ViewHolder holder, int position) {
        final My_Log_List vendor_list = my_log_lists.get(position);

        holder.Log_Village_Name.setText(vendor_list.getVilage_Name());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView Log_Village_Name;
        public ViewHolder(View itemView) {

            super(itemView);


            Log_Village_Name= (TextView) itemView.findViewById(R.id.My_Log_Village_Name);
        }
    }
}
