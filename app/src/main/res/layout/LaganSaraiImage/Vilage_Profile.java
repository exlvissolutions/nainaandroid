package com.exlvis.LaganSaraiImage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class Vilage_Profile extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    String Village_EditProfile = "http://www.lagansarai.com/Service.svc/VillageData_Edit";
    String Village_UpdateProfile = "http://www.lagansarai.com/Service.svc/Profile_Upadte";
    SharedPreferences sharedPref;
    private String file_name = "Users_Details";
    String V_Pass_Word,V_Mobile_number,V_Email,V_u_name,V_Location,V_Proff;
    EditText village_Mem_pass,Village_Mem_Email,Village_Location,Village_Proff;
    TextView Village_Mem_Name,Village_Mem_mob;
    Button button;
    private String TAG = "Vilage_Profile";
    String Log_id;
    public static String type;
    public static String status;
    Spinner spinner1;
     List<String> list1;
    String getSpinner1;
    ArrayAdapter<String> adapter11;
    ProgressDialog progressDialog;
    private String requestBody;
    JSONObject jsonObject1 = new JSONObject();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vilage__profile);

        progressDialog = new ProgressDialog(com.exlvis.LaganSaraiImage.Vilage_Profile.this);
        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        // userName = sharedPref.getString("Username","default");
        // loginStat = sharedPref.getString("Login_Status","default");
        // userType = sharedPref.getString("UserType","default");
        Log_id = sharedPref.getString("login_id","default");


        Log.d(TAG,"Log_ID "+Log_id);

        Village_Mem_Name= (TextView) findViewById(R.id.Village_name);

        Village_Location= (EditText) findViewById(R.id.Village_Location);

        Village_Mem_mob= (TextView) findViewById(R.id.Village_Mobile);

        village_Mem_pass= (EditText) findViewById(R.id.Village_Pass);

        Village_Mem_Email= (EditText) findViewById(R.id.Village_Email);
        button= (Button) findViewById(R.id.Village_Update_Button);

        //Village_Proff= (EditText) findViewById(R.id.Village_Prof);




         list1 = new ArrayList<String>();


       // list1.add(type);
        list1.add("Engineer");
        list1.add("Doctor");
        list1.add("Gov Employee");
        list1.add("CIDCO Empoyee");
        list1.add("Other");

        Log.d("type3",""+type);

        spinner1 = (Spinner) findViewById(R.id.Spinner_Prof1);
        spinner1.setOnItemSelectedListener(this);
        adapter11 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list1);
        spinner1.setSelection(list1.indexOf(type));
        adapter11.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
        //spinner1.setAdapter(adapter11);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                V_u_name = Village_Mem_Name.getText().toString().trim();
                V_Location = Village_Location.getText().toString().trim();
                V_Mobile_number = Village_Mem_mob.getText().toString().trim();
                V_Pass_Word = village_Mem_pass.getText().toString().trim();
                V_Email = Village_Mem_Email.getText().toString().trim();
//                V_Proff = Village_Proff.getText().toString().trim();
                getSpinner1 = spinner1.getSelectedItem().toString();
                Update_Profile();
            }
        });

        //progressDialog.setMessage("Please Wait");
        //progressDialog.show();
        //showProgress(true);

        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        try {




            jsonObject1.put("Log_id", Log_id);





            //jsonObject.put("UserType", UserType);
            Log.d("JSONObject", "" + jsonObject1);
            this.requestBody = jsonObject1.toString();
            Log.d("requestBody", "" + requestBody);


        } catch (JSONException e) {

            Log.d("JSON Exception", " " + e);

        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Village_EditProfile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "Login JSON" + response);



                try {



                    for (int i = 0; i < response.length(); i++) {
                        JSONArray jSONArray = new JSONArray(response);
                        JSONObject json = jSONArray.getJSONObject(i);


                         status = json.getString("status");
                        type = json.getString("prof");
                      //  String Prof = json.getString("prof");
                       // Village_Mem_Name.setText(Prof);

                        String U_Name = json.getString("name");
                        Village_Mem_Name.setText(U_Name);

                        String e_mail= json.getString("email");
                        Village_Mem_Email.setText(e_mail);

                        String Location= json.getString("location");
                        Village_Location.setText(Location);

                        String Mob_no = json.getString("mobile");
                        Village_Mem_mob.setText(Mob_no);

                        String pass_word = json.getString("password");
                        village_Mem_pass.setText(pass_word);

                      //  String Proff = json.getString("prof");
                       // Village_Proff.setText(Proff);



                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                list1.clear();
                Log.d("type1111111111", "1111111111111" + type);


                if (type.equals("Engineer")){

                    list1.add(type);
                    list1.add("Doctor");
                    list1.add("CIDCO Empoyee");
                    list1.add("Gov Employee");
                    list1.add("Other");

                } else  if (type.equals("Doctor")){
                    list1.add(type);
                    list1.add("Engineer");
                    list1.add("CIDCO Empoyee");
                    list1.add("Gov Employee");
                    list1.add("Other");
                }
                else  if (type.equals("CIDCO Empoyee")){
                    list1.add(type);
                    list1.add("Engineer");
                    list1.add("Doctor");
                    list1.add("Gov Employee");
                    list1.add("Other");
                }
                else  if (type.equals("Other")){
                    list1.add(type);
                    list1.add("Engineer");
                    list1.add("Doctor");
                    list1.add("Gov Employee");
                    list1.add("CIDCO Empoyee");
                    list1.add("Other");
                }
                if (status.equals("1")) {
                    showProgress(false);

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Vilage_Profile.this, "Profile Update Successfully ", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();

                    return;
                }

                progressDialog.dismiss();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();


                Toast.makeText(com.exlvis.LaganSaraiImage.Vilage_Profile.this,"Network ErrorPlease Try again...", Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        com.exlvis.LaganSaraiImage.MySingleton.getInstance(com.exlvis.LaganSaraiImage.Vilage_Profile.this).addToRequest(stringRequest);


        spinner1.setAdapter(adapter11);
    }

    private void Update_Profile() {

        V_u_name = Village_Mem_Name.getText().toString().trim();
        V_Location = Village_Location.getText().toString().trim();
        V_Mobile_number = Village_Mem_mob.getText().toString().trim();
        V_Pass_Word = village_Mem_pass.getText().toString().trim();
        V_Email = Village_Mem_Email.getText().toString().trim();
//        V_Proff = Village_Proff.getText().toString().trim();
        getSpinner1 = spinner1.getSelectedItem().toString();


        if (getSpinner1.equals(null) || getSpinner1.equals("Select Type")){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Vilage_Profile.this, "Please Select Profession", Toast.LENGTH_LONG);
            toast.setGravity(17, 0, 0);
            toast.show();
            spinner1.requestFocus();
            return;
        }
        if (V_Pass_Word.equals(null) || V_Pass_Word.equals("")){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Vilage_Profile.this, "Password field cannot be blank.", Toast.LENGTH_LONG);
            toast.setGravity(17, 0, 0);
            toast.show();
            village_Mem_pass.requestFocus();
            return;
        }
        if (V_Mobile_number.equals("") || V_Mobile_number.length()<10){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Vilage_Profile.this, " Enter 10 digit mob number ", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
            return;
        }

        if (!(isValidEmail_id(V_Email))){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Vilage_Profile.this, "Please enter valid email.", Toast.LENGTH_LONG);
            toast.setGravity(17, 0, 0);
            toast.show();
            Village_Mem_Email.requestFocus();
            return;
        }


        showProgress(true);

        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        try {



            jsonObject1.put("name", V_u_name);

            jsonObject1.put("location", V_Location);

            jsonObject1.put("password", V_Pass_Word);

            jsonObject1.put("email", V_Email);

            jsonObject1.put("log_id", Log_id);


            jsonObject1.put("mobile", V_Mobile_number);

            jsonObject1.put("prof", getSpinner1);

            Log.d("V_u_name", "" + V_u_name);
            Log.d("V_Location", "" + V_Location);
            Log.d("V_Pass_Word", "" + V_Pass_Word);
            Log.d("V_Email", "" + V_Email);
            Log.d("Log_id", "" + Log_id);

            Log.d("getSpinner1", "" + getSpinner1);
            Log.d("V_Mobile_number", "" + V_Mobile_number);

            Log.d("JSONObject", "" + jsonObject1);

            this.requestBody = jsonObject1.toString();
            Log.d("requestBody", "" + requestBody);


        } catch (JSONException e) {

            Log.d("JSON Exception", " " + e);

        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Village_UpdateProfile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "Login JSON" + response);


                try {
                    JSONArray jSONArray2 = new JSONArray(response);
                    JSONObject jsonObject2 = jSONArray2.getJSONObject(0);
                   status = jsonObject2.getString("status");


                    String loginStatus = jsonObject2.getString("status");
                    //String loginStatus = jsonObject1.getString("Status");
                    Log.d("loginStatus", "" + loginStatus);
                    if (loginStatus.equals("1")) {
                        showProgress(false);

                        Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Vilage_Profile.this, "Profile Update Successfully ", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();

                        return;
                    }
                    if (status.equals("0")) {
                        showProgress(false);
                        Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Vilage_Profile.this, "Please try again", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                        return;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        com.exlvis.LaganSaraiImage.MySingleton.getInstance(com.exlvis.LaganSaraiImage.Vilage_Profile.this).addToRequest(stringRequest);







    }

    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");

            return;
        }
        Log.d("Inside Showprogress", "Elsepart");

    }

    public final static boolean isValidEmail_id(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
