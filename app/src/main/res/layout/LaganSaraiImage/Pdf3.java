package com.exlvis.LaganSaraiImage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class Pdf3 extends AppCompatActivity {
    PDFView pdfView3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf3);

        pdfView3 = (PDFView) findViewById(R.id.pdfView3);

        pdfView3.fromAsset("N3.pdf").load();
    }
}
