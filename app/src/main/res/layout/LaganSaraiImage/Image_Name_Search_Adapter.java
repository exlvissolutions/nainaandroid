package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class Image_Name_Search_Adapter  extends RecyclerView.Adapter<com.exlvis.LaganSaraiImage.Image_Name_Search_Adapter.ViewHolder> {
    Context context;

    ArrayList<com.exlvis.LaganSaraiImage.Image_Name_Page> image_name_pages;

    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onViewButton(int position);


    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
    public Image_Name_Search_Adapter(ArrayList<com.exlvis.LaganSaraiImage.Image_Name_Page> image_name_pages1, Context context) {

        super();

        this.image_name_pages = image_name_pages1;
        this.context = context;
    }
    @Override
    public com.exlvis.LaganSaraiImage.Image_Name_Search_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_search_adapter, parent, false);
   ViewHolder viewHolder = new ViewHolder(v);
   return viewHolder;
    }

    @Override
    public void onBindViewHolder(com.exlvis.LaganSaraiImage.Image_Name_Search_Adapter.ViewHolder holder, int position) {
        final com.exlvis.LaganSaraiImage.Image_Name_Page namePage = image_name_pages.get(position);
        holder.Vilage_name.setText(namePage.getVilage_name());

        holder.Vilage_ID.setText(namePage.getVil_ID());
    }

    @Override
    public int getItemCount() {
        return image_name_pages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView Vilage_name;
        TextView Vilage_ID;
        Button ViewButton;
        public ViewHolder(View itemView) {
            super(itemView);
            Vilage_name = (TextView) itemView.findViewById(R.id.Vilage_Name);

            Vilage_ID = (TextView) itemView.findViewById(R.id.Vilage_ID);

            ViewButton= (Button) itemView.findViewById(R.id.view_button);

            ViewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onViewButton(position);
                        }
                    }
                }
            });
        }
    }

    public void filterList(ArrayList<com.exlvis.LaganSaraiImage.Image_Name_Page> filteredList) {

        image_name_pages = filteredList;
        notifyDataSetChanged();
    }
}
