package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by bhushandc on 18/12/17.
 */

public class WebviewClient extends WebViewClient {
    private final Context context;
    private final Object instanceOfClass;

    public WebviewClient(Context context, Object instanceOfClass) {
        this.context = context;
        this.instanceOfClass = instanceOfClass;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        if (Build.VERSION.SDK_INT < 21) {
            return super.shouldOverrideUrlLoading(view, request);
        }
        view.loadUrl(request.getUrl().toString().trim());
        return true;
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if (WebViewActivity.mWebviewProgressBar != null) {
            WebViewActivity.mWebviewProgressBar.bringToFront();
            WebViewActivity.mWebviewProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);
    }

    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (WebViewActivity.mWebview != null) {
            WebViewActivity.mWebview.bringToFront();
        }
        WebViewActivity.mWebviewProgressBar.setVisibility(View.GONE);
    }

    public void onPageCommitVisible(WebView view, String url) {
        super.onPageCommitVisible(view, url);
    }


}
