package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class Information_Adapter extends RecyclerView.Adapter<com.exlvis.LaganSaraiImage.Information_Adapter.ViewHolder> {

    Context context;

    ArrayList<com.exlvis.LaganSaraiImage.Information_List> information_lists;

    private com.exlvis.LaganSaraiImage.Information_Adapter.OnItemClickListener onItemClickListener;


    public interface OnItemClickListener {

        void button1(int position);

    }


    public void setOnItemClickListener(com.exlvis.LaganSaraiImage.Information_Adapter.OnItemClickListener listener) {
        onItemClickListener =  listener;
    }

    public Information_Adapter(ArrayList<com.exlvis.LaganSaraiImage.Information_List> information_lists1, Context context) {

        super();

        this.information_lists = information_lists1;
        this.context = context;
    }


    @Override
    public com.exlvis.LaganSaraiImage.Information_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.information_adapter, parent, false);

ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(com.exlvis.LaganSaraiImage.Information_Adapter.ViewHolder holder, int position) {
        final com.exlvis.LaganSaraiImage.Information_List vendor_list = information_lists.get(position);
        holder.Noti_Title1.setText(vendor_list.getInfo_Title());
        holder.Noti_Desc1.setText(vendor_list.getInfo_Title());
    }

    @Override
    public int getItemCount() {
        return information_lists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView Noti_Title1;
        public TextView Noti_Desc1;
        public Button ViewButton1;
        public ViewHolder(View itemView) {
            super(itemView);

            Noti_Title1= (TextView) itemView.findViewById(R.id.noti_title1);
            Noti_Desc1 = (TextView) itemView.findViewById(R.id.noti_Desc1);
            ViewButton1 = (Button) itemView.findViewById(R.id.Navi_button1);

            ViewButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            onItemClickListener.button1(position);
                        }
                    }
                }
            });
        }
    }
}
