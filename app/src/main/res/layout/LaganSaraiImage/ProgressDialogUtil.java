package com.exlvis.LaganSaraiImage;

import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

/**
 * Created by bhushandc on 30/12/17.
 */

public class ProgressDialogUtil {

    public static void showProgressDialog(ProgressBar pDialog) {

        Log.d("ProgressDialogUtil","showProgressDialog called ");
        pDialog.setVisibility(View.VISIBLE);
    }

    public static void hideProgressDialog(ProgressBar pDialog) {

        Log.d("ProgressDialogUtil","hideProgressDialog called ");
        pDialog.setVisibility(View.GONE);
    }

}
