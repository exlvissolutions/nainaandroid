package com.exlvis.LaganSaraiImage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class Pdf1 extends AppCompatActivity {
    PDFView pdfView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf1);

        pdfView1 = (PDFView) findViewById(R.id.pdfView);

        pdfView1.fromAsset("N11.pdf").load();
    }
}
