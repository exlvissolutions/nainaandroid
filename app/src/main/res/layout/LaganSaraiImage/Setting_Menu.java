package com.exlvis.LaganSaraiImage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

public class Setting_Menu extends AppCompatActivity {
TextView profile,notifiaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting__menu);

        profile= (TextView) findViewById(R.id.profile_id);
        notifiaction= (TextView) findViewById(R.id.Notification_id);

        CardView cardView = (CardView) findViewById(R.id.card_view);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Setting_Menu.this,Setting.class);
                startActivity(intent);
            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Setting_Menu.this,Setting.class);
                startActivity(intent);
            }
        });
        notifiaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Setting_Menu.this,Setting_Notification_Activity.class);
                startActivity(intent);
            }
        });
    }
}
