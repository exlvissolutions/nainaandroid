package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.webkit.WebChromeClient;

/**
 * Created by bhushandc on 18/12/17.
 */

public class WebViewChromeClient extends WebChromeClient {
    private final Context context;
    private final Object instanceOfClass;

    public WebViewChromeClient(Context context, Object instanceOfClass) {
        this.context = context;
        this.instanceOfClass = instanceOfClass;
    }

}
