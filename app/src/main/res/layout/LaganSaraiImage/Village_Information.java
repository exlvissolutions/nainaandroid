package com.exlvis.LaganSaraiImage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;

public class Village_Information extends AppCompatActivity {


    PDFView pdfView,pdfView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_village__information);

       TextView profile= (TextView) findViewById(R.id.profile_id);
         pdfView = (PDFView) findViewById(R.id.pdfView);





        CardView cardView = (CardView) findViewById(R.id.card_view);
        CardView cardView2 = (CardView) findViewById(R.id.card_view2);
        CardView cardView3 = (CardView) findViewById(R.id.card_view3);
        CardView cardView4 = (CardView) findViewById(R.id.card_view4);
        CardView cardView5 = (CardView) findViewById(R.id.card_view5);
        CardView cardView6 = (CardView) findViewById(R.id.card_view6);
        CardView cardView7 = (CardView) findViewById(R.id.card_view7);


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Village_Information.this, com.exlvis.LaganSaraiImage.Pdf2.class);
                startActivity(intent);
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Village_Information.this, com.exlvis.LaganSaraiImage.Pdf1.class);
                startActivity(intent);
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Village_Information.this,Pdf3.class);
                startActivity(intent);
            }
        });


        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Village_Information.this,Pdf4.class);
                startActivity(intent);
            }
        });


        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Village_Information.this, com.exlvis.LaganSaraiImage.Pdf5.class);
                startActivity(intent);
            }
        });


        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Village_Information.this, com.exlvis.LaganSaraiImage.Pdf6.class);
                startActivity(intent);
            }
        });


        cardView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(com.exlvis.LaganSaraiImage.Village_Information.this, com.exlvis.LaganSaraiImage.Pdf7.class);
                startActivity(intent);
            }
        });


    }


}

