package com.exlvis.LaganSaraiImage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class Pdf5 extends AppCompatActivity {
    PDFView pdfView5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf5);

        pdfView5 = (PDFView) findViewById(R.id.pdfView5);

        pdfView5.fromAsset("N5.pdf").load();
    }
}
