package com.exlvis.LaganSaraiImage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class Village_ContactUs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_village__contact_us);

        TextView Url_link = (TextView) findViewById(R.id.link_tv1);

        Url_link.setMovementMethod(LinkMovementMethod.getInstance());

    }
}
