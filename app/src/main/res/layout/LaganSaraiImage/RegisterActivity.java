package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.TimeZone;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    Spinner spinner1;

    String getSpinner1;

    ArrayAdapter<CharSequence> adapter11;
    private EditText etSociety, etName, etMobile, etEmailID, etLocation, etUserName,etConfirmPassword,etPassword;
    private Button btRegSubmit;
    private RadioGroup regRadioGrp;
    private RadioButton rbResidentReg, rbStaffReg;
    private String userType;
    private String Society, Name, Mobile, EmailID, Location,requestBody,Username,Password,ConfirmPasswd;
    private final String REGISTER_URL = "http://www.lagansarai.com/Service.svc/Register";
    JSONObject jsonObject;
    private TextView Sign_in_text;
    private ProgressBar mProgressBar;
    private ScrollView mRegScrollView;
    TimeZone tz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initialization();

        spinner1.setOnItemSelectedListener(this);

        adapter11 = ArrayAdapter.createFromResource(this,R.array.proff,
                android.R.layout.simple_spinner_item);
        adapter11.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        spinner1.setAdapter(adapter11);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });








        Calendar cal = Calendar.getInstance();
        tz = cal.getTimeZone();

        btRegSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

               // Society = etSociety.getText().toString().trim();
                Name = etName.getText().toString().trim();
                getSpinner1 = spinner1.getSelectedItem().toString();
               // Username = etUserName.getText().toString().trim();
                Password = etPassword.getText().toString().trim();
                ConfirmPasswd = etConfirmPassword.getText().toString().trim();
                Mobile = etMobile.getText().toString().trim();
                EmailID = etEmailID.getText().toString().trim();
                Location = etLocation.getText().toString().trim();

                // Editexts Validations


                if (Name.equals(null) || Name.equals("")){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Name field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etName.requestFocus();
                    return;
                }
                if (getSpinner1.equals(null) || getSpinner1.equals("Select Type")){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Please Select Profession", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    spinner1.requestFocus();
                    return;
                }
                if (Password.equals(null) || Password.equals("")){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Password field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etPassword.requestFocus();
                    return;
                }if (ConfirmPasswd.equals(null) || ConfirmPasswd.equals("")){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Confirm Password field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etConfirmPassword.requestFocus();
                    return;
                }if (!(ConfirmPasswd.equals(Password))){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Confirm Password and Password does not match.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etConfirmPassword.requestFocus();
                    return;
                }
                if (Mobile.equals(null) || Mobile.equals("")){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Mobile field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etMobile.requestFocus();
                    return;
                }
                if (Mobile.length()<10){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Enter valid mobile number.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etMobile.requestFocus();
                    return;
                }
                if (EmailID.equals(null) || EmailID.equals("")){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Email field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etEmailID.requestFocus();
                    return;
                }
                if (!(isValidEmail(EmailID))){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Please enter valid email.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etEmailID.requestFocus();
                    return;
                }

                if (Location.equals(null) || Location.equals("")){

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Location field cannot be blank.", Toast.LENGTH_LONG);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    etLocation.requestFocus();
                    return;
                }

                else {

                    registerJSON(Society,Name,Mobile,Username,Password,EmailID,Location);
                    btRegSubmit.setEnabled(false);
                }
            }
        });

        Sign_in_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(com.exlvis.LaganSaraiImage.RegisterActivity.this,LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }


    private void initialization() {

       // etSociety = (EditText)findViewById(R.id.etSocietyName);
        etName = (EditText)findViewById(R.id.etName);
        spinner1 = (Spinner) findViewById(R.id.Spinner_Prof);
        etMobile = (EditText)findViewById(R.id.etregMobile);
        etEmailID = (EditText)findViewById(R.id.etEmail);
        etLocation = (EditText)findViewById(R.id.etLocation);
       // etUserName = (EditText)findViewById(R.id.etregUserName);
        etPassword = (EditText)findViewById(R.id.etregPassword);
        etConfirmPassword = (EditText)findViewById(R.id.etregConfirmPassword);
        btRegSubmit = (Button) findViewById(R.id.btRegSubmit);
        Sign_in_text = (TextView) findViewById(R.id.tvSignInText);
        mProgressBar = (ProgressBar) findViewById(R.id.registerProgressBar);
        mRegScrollView = (ScrollView) findViewById(R.id.regScroll);

        jsonObject = new JSONObject();

    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    //TODO:- Register JSON

    private void registerJSON(String society, String name, String mobile, String username, String password, String emailID, String location) {

        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        showProgress(true);

        try {

            jsonObject.put("name", name);

            jsonObject.put("prof", getSpinner1);
            // jsonObject.put("name", name);
            // jsonObject.put("Username", username);
            jsonObject.put("password", password);
            jsonObject.put("mobile", mobile);
            jsonObject.put("Email", emailID);
            jsonObject.put("Location", location);
            // jsonObject.put("timezone", tz.getDisplayName());

            Log.d("getSpinner1", "" + getSpinner1);
            Log.d("JSONObject", "" + jsonObject);
            this.requestBody = jsonObject.toString();

        } catch (JSONException e) {

            Log.d("JSON Exception", " " + e);

        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "" + response);
                try {

                    showProgress(false);

                    JSONArray jSONArray = new JSONArray(response);
                    int len = jSONArray.length();
                    JSONObject jsonObject1 = jSONArray.getJSONObject(0);

                    String loginStatus = jsonObject1.getString("status");
                    Log.d("loginStatus", "" + loginStatus);
                    String msg = jsonObject1.getString("Message");
                    Log.d("msg", "" + msg);

                    if (loginStatus.equals("3")) {
                        //Login Successful

                        showProgress(false);
                        Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this," "+msg,Toast.LENGTH_LONG);
                        toast.setGravity(17, 0, 0);
                        toast.show();

                        Intent intent = new Intent(com.exlvis.LaganSaraiImage.RegisterActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        return;
                    }
                    if (loginStatus.equals("1")) {
                        //mob already exists
                        showProgress(false);
                        Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this," "+msg,Toast.LENGTH_LONG);
                        toast.setGravity(17, 0, 0);
                        toast.show();
                        etName.setText("");
                        etLocation.setText("");
                        etEmailID.setText("");
                        etMobile.setText("");
                        etPassword.setText("");
                        etConfirmPassword.setText("");
                        etMobile.requestFocus();
                        btRegSubmit.setEnabled(true);

                        return;
                    }if (loginStatus.equals("2")) {
                        //email already exists
                        showProgress(false);
                        Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this," "+msg,Toast.LENGTH_LONG);
                        toast.setGravity(17, 0, 0);
                        toast.show();
                        etEmailID.requestFocus();
                        btRegSubmit.setEnabled(true);

                        return;
                    }
                    if (loginStatus.equals("5")) {
                        //username already exists
                        showProgress(false);
                        Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this," "+msg,Toast.LENGTH_LONG);
                        toast.setGravity(17, 0, 0);
                        toast.show();
                        etEmailID.requestFocus();
                        btRegSubmit.setEnabled(true);

                        return;
                    }

                    showProgress(false);

                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.RegisterActivity.this, "Network Error. Please try again.", Toast.LENGTH_SHORT);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    btRegSubmit.setEnabled(true);
                    Log.e("LoginActivity", "onResponse: Response from Server: " + response);

                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        MySingleton.getInstance(com.exlvis.LaganSaraiImage.RegisterActivity.this).addToRequest(stringRequest);

    }

    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");
            this.mProgressBar.setVisibility(View.VISIBLE);
            this.mProgressBar.bringToFront();
            //this.mRegScrollView.setVisibility(View.GONE);
            return;
        }
        Log.d("Inside Showprogress", "Elsepart");
        this.mProgressBar.setVisibility(View.GONE);
        this.mRegScrollView.setVisibility(View.VISIBLE);
        this.mRegScrollView.bringToFront();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
