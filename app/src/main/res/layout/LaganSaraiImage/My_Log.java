package com.exlvis.LaganSaraiImage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class My_Log extends AppCompatActivity {
    ProgressDialog progressDialog;
    private com.exlvis.LaganSaraiImage.My_Log_Adapter my_log_adapter;
    private String Log_Name_URL = "http://www.lagansarai.com/Service.svc/User_Log";
    ArrayList<My_Log_List> myLogLists;

    RecyclerView recyclerView;
JSONObject jsonObject = new JSONObject();
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    String S_Village_name;
    String Log_id,requestBody;
    RecyclerView.Adapter recyclerViewadapter;
    SharedPreferences sharedPref;
    private String file_name = "Users_Details";
    private String TAG = "My_Log";
    String Log_ID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__log);
        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        // userName = sharedPref.getString("Username","default");
        // loginStat = sharedPref.getString("Login_Status","default");
        // userType = sharedPref.getString("UserType","default");
        Log_ID = sharedPref.getString("login_id","default");


        Log.d(TAG,"Log_ID "+Log_ID);


        S_Village_name= "vName";

        progressDialog = new ProgressDialog(com.exlvis.LaganSaraiImage.My_Log.this);
        myLogLists = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.My_Log_recyclerview);



        recyclerView.setHasFixedSize(true);

        recyclerViewlayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(recyclerViewlayoutManager);

        progressDialog.setMessage("Please Wait");
        progressDialog.show();
        showProgress(true);

        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        try {



            jsonObject.put("log_id",Log_id );



            Log.d("", "" + Log_id);

            //jsonObject.put("UserType", UserType);
            Log.d("JSONObject", "" + jsonObject);
            this.requestBody = jsonObject.toString();
            Log.d("requestBody", "" + requestBody);


        } catch (JSONException e) {

            Log.d("JSON Exception", " " + e);

        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Log_Name_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "Login JSON" + response);

                try {



                    for (int i = 0; i < response.length(); i++) {
                        JSONArray jSONArray = new JSONArray(response);
                        JSONObject json = jSONArray.getJSONObject(i);




                        My_Log_List list = new My_Log_List();

                        list.setVilage_Name(json.getString(S_Village_name));


                        myLogLists.add(list);




                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }




                my_log_adapter = new com.exlvis.LaganSaraiImage.My_Log_Adapter((ArrayList<My_Log_List>) myLogLists, com.exlvis.LaganSaraiImage.My_Log.this);

               // complaint_details_adapter.setOnItemClickListener(My_Log.this);

                Log.d("recycleview", String.valueOf(my_log_adapter));
                recyclerView.setAdapter(my_log_adapter);

                Log.d("recycleview", String.valueOf(recyclerView));
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();

                Toast.makeText(com.exlvis.LaganSaraiImage.My_Log.this,"Network Error...", Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        MySingleton.getInstance(com.exlvis.LaganSaraiImage.My_Log.this).addToRequest(stringRequest);







    }

    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");

            return;
        }
        Log.d("Inside Showprogress", "Elsepart");

    }

    }

