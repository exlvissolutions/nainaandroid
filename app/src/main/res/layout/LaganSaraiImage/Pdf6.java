package com.exlvis.LaganSaraiImage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class Pdf6 extends AppCompatActivity {
    PDFView pdfView6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf6);

        pdfView6 = (PDFView) findViewById(R.id.pdfView6);

        pdfView6.fromAsset("N6.pdf").load();
    }
}
