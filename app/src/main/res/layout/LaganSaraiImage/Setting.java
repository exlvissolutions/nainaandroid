package com.exlvis.LaganSaraiImage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class Setting extends AppCompatActivity {

    TextView name, u_name,add_ress,landline,floortv,typetv,build,wingtv,Flat_No;
    EditText Pass_Word,Mobile_number,Email;
    ArrayAdapter<String> mem_mobile;
    String S_Pass_Word,S_Mobile_number,S_Email,S_name, S_u_name,S_flatno,S_add_ress,S_landline,S_floortv,S_typetv,S_build,S_wingtv;
    JSONObject jsonObject1 = new JSONObject();
    Button Update;
    private SharedPreferences sharedPreferences;
    String HttpUrl = "http://contractpro.in/service/Service.svc/edit_Member";
    String Update_URL = "http://contractpro.in/service/Service.svc/Update_Member";
    private String userName,loginStat,userType,soc_ID,type,mem_id;
    String Smember_typ, Smem_name, Su_name,SPass_Word, SMobile_number,SAdhar_no,SPVC,SDesi,SDriving_lic, SMember_ID,SPan_No, SEmail_id, SFlat_No, Swing_no, Sfloor_name, SBuilding_name,SLand_line,SAddress;
    private String TAG = "Setting";

    SharedPreferences sharedPref;
    private String file_name = "Users_Details";

    String memid,log_id;
    public static final String EXTRA_Upload_Login_Id = "username";
    public static final String EXTRA_Upload_Pass = "password";
    ProgressDialog progressDialog;
    private String requestBody;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        sharedPreferences = getSharedPreferences(file_name,Context.MODE_PRIVATE);

        progressDialog = new ProgressDialog(com.exlvis.LaganSaraiImage.Setting.this);
        sharedPref = getSharedPreferences(file_name, Context.MODE_PRIVATE);
        userName = sharedPref.getString("Username","default");
        loginStat = sharedPref.getString("Login_Status","default");
        mem_id = sharedPref.getString("member_id","default");
        soc_ID = sharedPref.getString("Society_ID","default");
        log_id = sharedPref.getString("Log_ID","default");

        Log.d(TAG,"mem_id "+mem_id);
        Log.d(TAG,"loginStat "+loginStat);
        Log.d(TAG,"soc_ID "+soc_ID);
        showProgress(true);


        name= (TextView) findViewById(R.id.name);
        u_name= (TextView) findViewById(R.id.user_name);
        add_ress= (TextView) findViewById(R.id.addresstv);
        landline= (TextView) findViewById(R.id.landlinetv);
        floortv= (TextView) findViewById(R.id.floor);
        wingtv= (TextView) findViewById(R.id.wingtv);
        build= (TextView) findViewById(R.id.buildingtv);
        typetv= (TextView) findViewById(R.id.typetv);
        Flat_No= (TextView) findViewById(R.id.flat_no);

        Pass_Word = (EditText) findViewById(R.id.Passwordet);
        Mobile_number = (EditText) findViewById(R.id.mobileet);
        Email = (EditText) findViewById(R.id.emailet);

        Button button = (Button) findViewById(R.id.sutting_update);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mem_mobile = new ArrayAdapter<String>(com.exlvis.LaganSaraiImage.Setting.this,android.R.layout.select_dialog_singlechoice);
                S_Pass_Word = Pass_Word.getText().toString().trim();
                S_Mobile_number = Mobile_number.getText().toString().trim();
                S_Email=Email.getText().toString().trim();
                S_name=name.getText().toString().trim();
                S_u_name=u_name.getText().toString().trim();
                S_add_ress=add_ress.getText().toString().trim();
                S_flatno=Flat_No.getText().toString();
                S_landline=landline.getText().toString().trim();
                S_floortv=floortv.getText().toString().trim();
                S_build=build.getText().toString().trim();
                S_typetv=typetv.getText().toString().trim();
                S_wingtv=wingtv.getText().toString().trim();


                Update_Profile();

            }
        });

        progressDialog.setMessage("Please Wait");
        progressDialog.show();
        showProgress(true);

        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        try {




            jsonObject1.put("MemId", mem_id);





            //jsonObject.put("UserType", UserType);
            Log.d("JSONObject", "" + jsonObject1);
            this.requestBody = jsonObject1.toString();
            Log.d("requestBody", "" + requestBody);


        } catch (JSONException e) {

            Log.d("JSON Exception", " " + e);

        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "Login JSON" + response);



                try {



                    for (int i = 0; i < response.length(); i++) {
                        JSONArray jSONArray = new JSONArray(response);
                        JSONObject json = jSONArray.getJSONObject(i);


                        String status = json.getString("status");

                        String U_Name = json.getString("username");
                        u_name.setText(U_Name);

                        String e_mail= json.getString("email");
                        Email.setText(e_mail);

                        String f_no = json.getString("flat_no");
                        Flat_No.setText(f_no);

                        String Mob_no = json.getString("Mem_Mobile");
                        Mobile_number.setText(Mob_no);

                        String pass_word = json.getString("password");
                        Pass_Word.setText(pass_word);

                         String M_type = json.getString("mem_type");
                        typetv.setText(M_type);

                        String m_name = json.getString("Member_name");
                        name.setText(m_name);

                        String mob = json.getString("Mem_Mobile");
                        Mobile_number.setText(mob);

                        String wing = json.getString("wing");
                        wingtv.setText(wing);

                        String floor = json.getString("floor");
                        floortv.setText(floor);



                        String build_name = json.getString("blgname");
                        build.setText(build_name);


                        add_ress.setText("" + wing + "-" + f_no + "," + floor + "," + build_name );


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();


                Toast.makeText(com.exlvis.LaganSaraiImage.Setting.this,"Something Wrong Please try Again... ", Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        com.exlvis.LaganSaraiImage.MySingleton.getInstance(com.exlvis.LaganSaraiImage.Setting.this).addToRequest(stringRequest);







    }

    private void Update_Profile() {
        S_Pass_Word = Pass_Word.getText().toString().trim();
        S_Mobile_number = Mobile_number.getText().toString().trim();
        S_Email=Email.getText().toString().trim();
        S_name=name.getText().toString().trim();
        S_u_name=u_name.getText().toString().trim();
        S_add_ress=add_ress.getText().toString().trim();
        S_flatno=Flat_No.getText().toString();
        S_landline=landline.getText().toString().trim();
        S_floortv=floortv.getText().toString().trim();
        S_build=build.getText().toString().trim();
        S_typetv=typetv.getText().toString().trim();
        S_wingtv=wingtv.getText().toString().trim();

        if (S_Pass_Word.equals(null) || S_Pass_Word.equals("")){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Setting.this, "Password field cannot be blank.", Toast.LENGTH_LONG);
            toast.setGravity(17, 0, 0);
            toast.show();
            Pass_Word.requestFocus();
            return;
        }
        if (S_Mobile_number.equals("") || S_Mobile_number.length()<10){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Setting.this, " Enter 10 digit mob number ", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
            return;
        }

        if (!(isValidEmail_id(S_Email))){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.Setting.this, "Please enter valid email.", Toast.LENGTH_LONG);
            toast.setGravity(17, 0, 0);
            toast.show();
            Email.requestFocus();
            return;
        }
        showProgress(true);

        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        try {




            jsonObject1.put("password", S_Pass_Word);
            jsonObject1.put("email", S_Email);




            jsonObject1.put("Mem_Mobile", S_Mobile_number);



            jsonObject1.put("MemId", mem_id);
            jsonObject1.put("LogId", log_id);




            //jsonObject.put("UserType", UserType);
            Log.d("JSONObject", "" + mem_id);
            Log.d("JSONObject", "" + S_Pass_Word);
            Log.d("JSONObject", "" + S_Email);
            Log.d("JSONObject", "" + S_Mobile_number);
            Log.d("JSONObject", "" + log_id);
            this.requestBody = jsonObject1.toString();
            Log.d("requestBody", "" + requestBody);


        } catch (JSONException e) {

            Log.d("JSON Exception", " " + e);

        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Update_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "Login JSON" + response);


                try {
                    JSONArray jSONArray2 = new JSONArray(response);
                    JSONObject jsonObject2 = jSONArray2.getJSONObject(0);
                    String status = jsonObject2.getString("status");



                    if (status.equals("1")){

                        Toast.makeText(com.exlvis.LaganSaraiImage.Setting.this,"Update Successfully",Toast.LENGTH_LONG).show();

                        if (status.equals("1")) {

                            SharedPreferences.Editor editor = sharedPreferences.edit();

                            editor.putString("password", S_Pass_Word);
                            editor.putString("username", S_u_name);
                            Log.d("Pass_Word", "Login JSON" + S_Pass_Word);
                            Log.d("u_name", "Login JSON" + S_u_name);
                            editor.commit();



                            return;
                        }

                    }

                    else if (status.equals("0")){
                        Toast.makeText(com.exlvis.LaganSaraiImage.Setting.this,"Update Unsuccessfully",Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        com.exlvis.LaganSaraiImage.MySingleton.getInstance(com.exlvis.LaganSaraiImage.Setting.this).addToRequest(stringRequest);







    }

    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");

            return;
        }
        Log.d("Inside Showprogress", "Elsepart");

    }

    public final static boolean isValidEmail_id(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    }

