package com.exlvis.LaganSaraiImage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class Pdf2 extends AppCompatActivity {
    PDFView pdfView2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf2);
        pdfView2 = (PDFView) findViewById(R.id.pdfView2);

        pdfView2.fromAsset("N1.pdf").load();
    }
}
