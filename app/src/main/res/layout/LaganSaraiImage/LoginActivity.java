package com.exlvis.LaganSaraiImage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity {

    private EditText etUsername, etPassword;
    private Button btSubmit;
    private JSONObject jsonObject;
    private String requestBody, userType;
    private final String LOGIN_URL = "http://www.lagansarai.com/Service.svc/Logins";
    //private final String LOGIN_URL = "http://www.societyvisitors.com/api/jsonexample/sample";
    private String Uname, Passwd;
    private TextView tvRegText, tvForgotPasswd, tvUnableToLogin, tvChatNow;
    private SharedPreferences sharedPreferences;
    private String file_name = "Users_Details";
    private String TAG = "LoginActivity";
    private ProgressBar mProgressbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialization();

        sharedPreferences = getSharedPreferences(file_name,Context.MODE_PRIVATE);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                Log.d(TAG,"userType "+userType);

                Uname = etUsername.getText().toString().trim();
                Passwd = etPassword.getText().toString().trim();

                /*Intent intent = new Intent(LoginActivity.this, WebViewActivity.class);
                startActivity(intent);*/

                loginCheck(Uname,Passwd,userType);

            }
        });

        tvRegText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(com.exlvis.LaganSaraiImage.LoginActivity.this, com.exlvis.LaganSaraiImage.RegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        tvForgotPasswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(com.exlvis.LaganSaraiImage.LoginActivity.this, com.exlvis.LaganSaraiImage.ForgotPassword.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });



    }

    private void initialization() {

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btSubmit = (Button) findViewById(R.id.btSubmit);

        tvRegText = (TextView)findViewById(R.id.tvRegisterText);
        tvForgotPasswd = (TextView)findViewById(R.id.tvForgotPassword);
       // tvUnableToLogin = (TextView)findViewById(R.id.tvUnableToLogin);
        //tvChatNow = (TextView)findViewById(R.id.tvChatNow);
        jsonObject = new JSONObject();
        mProgressbar = (ProgressBar) findViewById(R.id.loginProgressBar);

        //etPassword.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    private void loginCheck(String username, String password, String UserType) {

        if (username.equals("") || username.equals(null)){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.LoginActivity.this,"Please enter UserName",Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            etUsername.requestFocus();
            return;

        }

        if (password.equals("") || password.equals(null)){

            Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.LoginActivity.this,"Please enter Password",Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            etPassword.requestFocus();
            return;

        }

        attemptLogin(UserType);

    }

    // TODO :-  Login JSON

    public void attemptLogin(final String UserType) {

        showProgress(true);

        RetryPolicy policy = new DefaultRetryPolicy(75000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        try {

            //jsonObject.put("Username", Uname);
            //jsonObject.put("Pass", Passwd);

            jsonObject.put("username", Uname);
            jsonObject.put("password", Passwd);
            //jsonObject.put("UserType", UserType);
            Log.d("JSONObject", "" + jsonObject);
            this.requestBody = jsonObject.toString();
            Log.d("requestBody", "" + requestBody);


        } catch (JSONException e) {

            Log.d("JSON Exception", " " + e);

        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response from server", "Login JSON" + response);
                try {

                    showProgress(false);

                    JSONArray jSONArray = new JSONArray(response);
                    int len = jSONArray.length();
                    JSONObject jsonObject1 = jSONArray.getJSONObject(0);
                    String loginStatus = jsonObject1.getString("status");
                    //String loginStatus = jsonObject1.getString("Status");
                    Log.d("loginStatus", "" + loginStatus);
                    String msg = jsonObject1.getString("Message");
                    String log_id = jsonObject1.getString("login_id");
                    String name = jsonObject1.getString("Name");
                    Log.d("log_id", "" + log_id);

                    Log.d("msg", "" + msg);


                    if (loginStatus.equals("1")) {
                        showProgress(false);
                         Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.LoginActivity.this, "Login Successfully ", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();


                        SharedPreferences.Editor editor = sharedPreferences.edit();
                       // editor.putString("Username",Uname);
                       // editor.putString("Password",Passwd);
                       // editor.putString("Login_Status","Y");
                       // editor.putString("UserType",type);
                      //  editor.putString("Society_ID",soc_id);
                        editor.putString("login_id",log_id);
                        editor.putString("Name",name);
                       // editor.putString("member_id",memid);
                        editor.commit();

                        Intent intent = new Intent(com.exlvis.LaganSaraiImage.LoginActivity.this, com.exlvis.LaganSaraiImage.LoggedAsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                       // intent.putExtra("UserType", type);
                        startActivity(intent);
                        finish();

                        return;
                    }
                    if (loginStatus.equals("2")) {
                        showProgress(false);
                        Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.LoginActivity.this, " "+msg, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                        return;
                    }

                    showProgress(false);
                    Toast toast = Toast.makeText(com.exlvis.LaganSaraiImage.LoginActivity.this, "Network Error. Please try again.", Toast.LENGTH_SHORT);
                    toast.setGravity(17, 0, 0);
                    toast.show();
                    Log.e("LoginActivity", "onResponse: Response from Server: " + response);

                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(policy);

        try {

        } catch (Exception e) {

            //Log.d("Stringrequest","Exception"+e);
        }

        com.exlvis.LaganSaraiImage.MySingleton.getInstance(com.exlvis.LaganSaraiImage.LoginActivity.this).addToRequest(stringRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void showProgress(boolean progress) {
        if (progress) {
            Log.d("Inside Showprogress", "Ifpart");
            mProgressbar.setVisibility(View.VISIBLE);
            mProgressbar.bringToFront();
            return;
        }
        Log.d("Inside Showprogress", "Elsepart");
        mProgressbar.setVisibility(View.GONE);
    }

}
