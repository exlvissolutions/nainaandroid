package com.exlvis.LaganSaraiImage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class Pdf4 extends AppCompatActivity {
    PDFView pdfView4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf4);

        pdfView4 = (PDFView) findViewById(R.id.pdfView4);

        pdfView4.fromAsset("N4.pdf").load();
    }
}
